import Bb from 'backbone';
import AttachedFileModel from './AttachedFileModel';

const AttachedFileCollection = Bb.Collection.extend({
    model: AttachedFileModel,
    /**
     * Custom parse method
     * Traite les données renvoyées par le serveur pour construire la collection
     */
    parse(xhrResponse) {
        if ('attachments' in xhrResponse) {
            return xhrResponse['attachments'];
        } else {
            return xhrResponse;
        }
    }
});
export default AttachedFileCollection;
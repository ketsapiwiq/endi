/*
 * File Name : ProductModel.js
 *
 */
import BaseModel from "base/models/BaseModel.js";
import Radio from 'backbone.radio';
import DuplicableMixin from 'base/models/DuplicableMixin.js';
import Validation from 'backbone-validation';
import {
    bindModelValidation,
    unbindModelValidation
} from 'backbone-tools.js';


const ProductModel = BaseModel.extend(DuplicableMixin).extend(Validation).extend({
    props: [
        'id',
        'type_',
        'label',
        'description',
        'supplier_ht',
        'margin_rate',
        "quantity",
        'ht',
        'total_ht',
        'unity',
        'tva_id',
        'product_id',
        "order",
        'chapter_id',
        'mode',
    ],
    validation: {
        'description': {
            required: true,
            msg: "Veuillez saisir une description"
        },
        ht: {
            required(value, attr, computedState) {
                return this.get('mode') == 'ht';
            },
            pattern: "amount",
            msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule",
        },
        supplier_ht: {
            required(value, attr, computedState) {
                return this.get('mode') == 'supplier_ht';
            },
            pattern: "amount",
            msg: "Veuillez saisir un coût d'achat, dans la limite de 5 chiffres après la virgule",
        },
        margin_rate: [{
                required: false,
                pattern: 'amount',
                msg: "Le coefficient de marge doit être un nombre entre 0 et 1, dans la limite de 5 chiffres après la virgule"
            },
            function (value) {
                value = parseInt(value.replace(',', '.'));
                if (value < 0 || value >= 1) {
                    return "Le coefficient de marge doit être un nombre entre 0 et 1"
                }

            }
        ],
        tva_id: {
            required: true,
            msg: 'Requis : Veuillez saisir le taux de TVA à appliquer'
        },
        product_id: {
            required: true,
            msg: 'Requis : Veuillez saisir le compte produit'
        },
    },
    defaults() {
        console.log("In Product model defaults");
        let config = Radio.channel('config');
        let form_defaults = config.request('get:options', 'defaults');
        this.user_session = Radio.channel('session');
        form_defaults['tva_id'] = this.user_session.request('get', 'tva_id', form_defaults['tva_id']);
        let product_id = this.user_session.request('get', 'product_id', '');
        if (product_id !== '') {
            form_defaults['product_id'] = product_id;
        }
        form_defaults['type_'] = 'price_study_product'
        form_defaults['quantity'] = 1
        form_defaults['mode'] = 'ht'
        return form_defaults;
    },
    initialize: function () {
        ProductModel.__super__.initialize.apply(this, arguments);
        this.facade = Radio.channel('priceStudyFacade');
        this.config = Radio.channel('config');
        this.user_session = Radio.channel('session');
        this.tva_options = this.config.request('get:options', 'tvas');
        this.product_options = this.config.request('get:options', 'products');
        this.on('saved', this.onSaved, this);
    },
    onSaved() {
        this.user_session.request('set', 'tva_id', this.get('tva_id'));
        this.user_session.request('set', 'product_id', this.get('product_id'));
    },
    product_object() {
        let product_id = parseInt(this.get('product_id'));
        return this.product_options.find((value) => product_id == value.id);
    },
    tva_object() {
        let tva_id = parseInt(this.get('tva_id'));
        return this.tva_options.find((value) => tva_id == value.id);
    },
    tva_label() {
        return this.findLabelFromId('tva_id', 'label', this.tva_options);
    },
    validateModel() {
        bindModelValidation(this);
        const result = this.validate() || {};
        unbindModelValidation(this);
        return result;
    },
});
export default ProductModel;
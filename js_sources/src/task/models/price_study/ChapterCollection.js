import OrderableCollection from 'base/models/OrderableCollection.js';
import ChapterModel from './ChapterModel';

const ChapterCollection = OrderableCollection.extend({
    model: ChapterModel,
});
export default ChapterCollection;
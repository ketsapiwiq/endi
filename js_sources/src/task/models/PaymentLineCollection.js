import OrderableCollection from "../../base/models/OrderableCollection.js";
import PaymentLineModel from './PaymentLineModel.js';

const PaymentLineCollection = OrderableCollection.extend({
    model: PaymentLineModel,
    url: function () {
        return AppOption['context_url'] + '/' + 'payment_lines';
    },
    validate: function () {
        var result = {};
        this.each(function (model) {
            var res = model.validate();
            if (res) {
                _.extend(result, res);
            }
        });
        return result;
    }
});
export default PaymentLineCollection;
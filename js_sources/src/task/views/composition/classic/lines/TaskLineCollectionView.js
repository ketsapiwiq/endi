import Mn from 'backbone.marionette';
import TaskLineView from './TaskLineView.js';

const TaskLineCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    className: 'lines',
    childView: TaskLineView,
    collectionEvents: {
        'change:reorder': 'render',
    },
    childViewOptions(model) {
        // Forward the edit option to the children
        return {
            edit: this.getOption('edit'),
            is_ttc_mode: this.getOption('is_ttc_mode'),
            section: this.getOption('section'),
        }
    },
    // Bubble up child view events
    childViewTriggers: {
        'edit': 'line:edit',
        'delete': 'line:delete'
    },
    childViewEvents: {
        'order:up': "onChildViewOrderUp",
        'order:down': "onChildViewOrderDown",
    },
    onChildViewOrderUp: function (childView) {
        console.log("Order up")
        this.collection.moveUp(childView.model);
    },
    onChildViewOrderDown: function (childView) {
        console.log("Order down")
        this.collection.moveDown(childView.model);
    }
});
export default TaskLineCollectionView;
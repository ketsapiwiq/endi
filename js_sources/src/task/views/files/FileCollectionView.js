import Mn from "backbone.marionette";
import FileView from "./FileView";


/**
 * Collection view for file representation. 
 * The childview can be specified on init
 * 
 * @fires 'file:updated' : when a file DL popup has been closed (file added/deleted/updated)
 */
const FileCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    childView: FileView,
    childViewTriggers: {
        "file:updated": "file:updated"
    }
});
export default FileCollectionView;
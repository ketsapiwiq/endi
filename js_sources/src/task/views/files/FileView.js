import Mn from 'backbone.marionette';

const template = require('./templates/FileView.mustache');

const FileView = Mn.View.extend({
    tagName: 'tr',
    template: template,
    ui: {
        view_button: ".btn-view",
    },
    events: {
        "click @ui.view_button": "onView",
    },
    modelEvents: {
        'change:name': 'render'
    },
    onFilePopupCallback(options) {
        this.triggerMethod('file:updated')
    },
    onView() {
        window.openPopup(
            "/files/" + this.model.get("id"), this.onFilePopupCallback.bind(this)
        );
    },
});
export default FileView
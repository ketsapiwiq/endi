import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import Totals from './Totals';
import Details from './Details';
import Contributions from './Contributions.js';

var template = require("./templates/ResumeView.mustache");

/**
 * Show totals
 */
const ResumeView = Mn.View.extend({
    className: "totals grand-total",
    template: template,
    regions: {
        'totals': '.totals'
    },
    modelEvents: {
        'change': 'render'
    },
    templateContext() {
        return {
            has_price_study: this.model.hasPriceStudy(),
            has_contribution: this.model.hasContributions()
        }
    },
    onRender() {
        if (this.model.hasPriceStudy()) {
            const price_study = this.model.get('price_study');
            this.addRegion('material', '.total-material');
            this.addRegion('labor', '.total-labor');
            this.showChildView('material', new Details({
                title: "Matériel",
                label: "Déboursé sec",
                data: price_study.material
            }));
            this.showChildView('labor', new Details({
                title: "Main d’œuvre",
                label: "Déboursé sec",
                data: price_study.labor
            }));
            if (this.model.hasContributions()) {
                this.addRegion('contribution', '.total-contribution');
                this.showChildView('contribution', new Contributions({
                    data: price_study.contributions
                }))
            }
        }
        this.showChildView('totals', new Totals({
            model: this.model
        }));
    },
});
export default ResumeView;
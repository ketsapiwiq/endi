import Mn from 'backbone.marionette';

import { hideLoader, showLoader } from 'tools';
import FormBehavior from '../../base/behaviors/FormBehavior.js';
import DateWidget from '../../widgets/DateWidget.js';
import InputWidget from '../../widgets/InputWidget.js';
import Select2Widget from '../../widgets/Select2Widget.js';
import Radio from 'backbone.radio';
import NodeFileViewerFactory
    from "../../common/components/NodeFileViewerFactory";


const BaseExpenseFormView = Mn.View.extend({
    behaviors: [FormBehavior],
    template: require('./templates/ExpenseFormView.mustache'),
    attributes: {
        class: "layout flex two_cols pdf_viewer",
    },
	regions: {
        'category': '.category',
		'date': '.date',
		'type_id': '.type_id',
		'description': '.description',
        'invoice_number': '.invoice_number',
		'ht': '.ht',
		'manual_ttc': '.manual_ttc',
		'tva': '.tva',
		'business_link': '.business_link',
        'supplier_id': '.supplier_id',
        'files': '.files',
        'preview': {el: '.preview', replaceElement: true},
    },
    // Bubble up child view events
    //
    childViewTriggers: {
        'change': 'data:modified',
    },
    childViewEvents: {
        'finish': 'onChildChange',
    },
    onBeforeSync: showLoader,
    onFormSubmitted: hideLoader,
    initialize(){
        // Common initialization.
        var channel = Radio.channel('config');
        this.type_options = this.getTypeOptions();
        this.suppliers_options = channel.request('get:options', 'suppliers');
        console.log('this.type_options', this.getTypeOptions());
        this.today = channel.request(
            'get:options',
            'today',
        );
        // If we have no type (eg: new expense form), adopt the first option of
        // the select list.
        if (this.model.get('type_id') === undefined) {
            this.model.set('type_id', String(this.type_options[0].id));
        }
    },
    shouldShowPreview(){
        return ((this.model.get('files') || []).length === 1);
    },
    showFilesSelect(){
        var channel = Radio.channel('facade');
        let attachments = channel.request('get:collection', 'attachments');
        const view = new Select2Widget({
            title: "Justificatifs",
            options: attachments.asSelectOptions(),
            field_name: "files",
            multiple: true,
            value: this.model.get('files'),
            placeholder: "Choisir un ou plusieurs justificatifs déjà téléversés",
        });
        this.showChildView('files', view);
    },
    showPreview(){
        let view;
        let files =  this.model.get('files');
        if (files && files.length > 0) {
            var channel = Radio.channel('facade');
            let attachments = channel.request('get:collection', 'attachments');
            let file = attachments.get(files[0]);
            view = NodeFileViewerFactory.getViewer(file);
        }
        if (view) {
            this.showChildView('preview', view);
        }
    },
    onRender(){
        var view;
        view = new DateWidget({
            date: this.model.get('date'),
            title: "Date",
            field_name: "date",
            default_value: this.today,
            required: true,
        });
        this.showChildView("date", view);

        view = new Select2Widget({
            value: this.model.get('supplier_id'),
            title: 'Fournisseur',
            field_name: 'supplier_id',
            options: this.suppliers_options,
            placeholder: 'Choisir un fournisseur',
        });
        this.showChildView('supplier_id', view);

        view = new InputWidget({
            value: this.model.get('invoice_number'),
            title: 'Numéro de la facture',
            field_name: 'invoice_number',
        });
        this.showChildView('invoice_number', view);

        let previousType = this.model.get('type_id');
        view = new Select2Widget({
            value: this.model.get('type_id'),
            title: 'Type de dépense',
            field_name: 'type_id',
            options: this.type_options,
            id_key: 'id',
            required: true,
        });
        this.showChildView('type_id', view);

        // Syncs model to allow proppper rendering of the form based on wether
        // we have TVA or not.
        this.triggerMethod('data:modified', 'type_id', view.getCurrentValues()[0])
        if (previousType != view.getCurrentValues()[0]) {
            /* re-render to get correct hide/show of amount fields
             * Handle cases where we changed tab and default type
             * option of the new tab has different field presence requirements (tva/ht/ttc)
             * than previous tab's one.
             * A bit hackish
             */
            this.render();
            return;
        }

        let htParams = {
            value: this.model.get('ht'),
            title: 'Montant HT',
            field_name: 'ht',
            addon: "€",
            required: true,
        }

        let tvaParams = {
            value: this.model.get('tva'),
            title: 'Montant TVA',
            field_name: 'tva',
            addon: "€",
            required: true,
        }

        let manual_ttcParams = {
            value: 0,
            title: 'Montant TTC',
            field_name: 'manual_ttc',
            addon: "€",
            required: true,
        }
        if (this.model.hasTvaOnMargin()) {
            tvaParams.value = 0;
            htParams.value = 0;

            manual_ttcParams.value = this.model.get('manual_ttc');

        } else if (! this.model.hasDeductibleTva()) {
            manual_ttcParams.value = 0;
            tvaParams.value = 0;
            htParams.title = 'Montant TTC'
        }

        view = new InputWidget(manual_ttcParams);
        this.showChildView('manual_ttc', view);
        view = new InputWidget(htParams);
        this.showChildView('ht', view);
        view = new InputWidget(tvaParams);
        this.showChildView('tva', view);
        this.showFilesSelect();
        if (this.shouldShowPreview()) {
            this.showPreview();
        }
    },
    onChildChange(field_name, value) {
        this.triggerMethod('data:modified', field_name, value);

        if (field_name == 'type_id') {
            this.render();
        } else if (field_name == 'files') {
            this.triggerMethod('preview:displayStatusChange', this.shouldShowPreview());
            this.render();
        }
    },
    afterSerializeForm(datas){
        let modifiedDatas = _.clone(datas);

        /* We also want the category to be pushed to server,
         * even if not present as form field
         */
        modifiedDatas['category'] = this.model.get('category');

        // Hack to allow setting those fields to null.
        // Otherwise $.serializeForm skips <select> with no value selected
        modifiedDatas['customer_id'] = this.model.get('customer_id');
        modifiedDatas['project_id'] = this.model.get('project_id');
        modifiedDatas['business_id'] = this.model.get('business_id');

        return modifiedDatas;
    },
    templateContext: function(){
        let hasTvaOnMargin = this.model.hasTvaOnMargin();
        return {
            title: this.getOption('title'),
            button_title: this.getOption('buttonTitle'),
            add: this.getOption('add'),
            hidden_ht: hasTvaOnMargin,
            hidden_tva: ! this.model.hasDeductibleTva() || hasTvaOnMargin,
            hidden_manual_ttc: ! hasTvaOnMargin,
            should_show_preview: this.shouldShowPreview(),
        };
    }
});
export default BaseExpenseFormView;

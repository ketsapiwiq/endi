import $ from 'jquery';
import _ from 'underscore';

import {applicationStartup} from '../backbone-tools.js';

import App from './components/App.js';
import Facade from './components/Facade.js';
import ToolbarApp from './components/ToolbarApp.js';
import ExpenseTypeService from "../common/components/ExpenseTypeService.js";
import AttachmentUploadService from "../common/components/AttachmentUploadService.js";
import StatusHistoryApp from "../common/components/StatusHistoryApp.js";

$(function(){
    applicationStartup(
        AppOption,
        App,
        Facade,
        {
            actionsApp: ToolbarApp,
            statusHistoryApp: StatusHistoryApp,
            customServices: [ExpenseTypeService, AttachmentUploadService],
        }
    );
});

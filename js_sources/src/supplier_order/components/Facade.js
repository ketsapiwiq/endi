import Mn from 'backbone.marionette';

import { ajax_call } from '../../tools.js';
import { getPercent } from '../../math.js';
import NodeFileCollection from '../../common/models/NodeFileCollection.js';
import TotalModel from '../models/TotalModel.js';
import SupplierOrderModel from '../models/SupplierOrderModel.js';
import SupplierOrderLineCollection from '../models/SupplierOrderLineCollection.js';
import FacadeModelApiMixin from "../../base/components/FacadeModelApiMixin";
import StatusLogEntryCollection
    from "../../common/models/StatusLogEntryCollection";


const FacadeClass = Mn.Object.extend(FacadeModelApiMixin).extend({
    channelName: 'facade',
    radioEvents: {
        "changed:line": "computeLineTotal",
        "changed:totals": "computeFundingTotals",
        "changed:order.cae_percentage": "computeFundingTotals",
    },
    radioRequests: {
        'get:collection': 'getCollectionRequest',
        'get:model': 'getModelRequest',
        'is:valid': "isDataValid",
        'save:all': 'saveAll',
    },
    initialize(options){
        this.models = {};
        this.collections = {};
    },
    start(){
        console.log("Starting the facade");
        let deferred = ajax_call(this.url);
        return deferred.then(this.setupModels.bind(this));
    },
    setup(options){
        console.log("Facade.setup");
        console.table(options);
        this.mergeOptions(options, ['edit']);
        this.url = options['context_url'];
    },
    setupModels(context_datas){
        this.models.total = new TotalModel();
        var lines = context_datas['lines'];
        this.collections.lines = new SupplierOrderLineCollection(lines);
        this.collections.attachments = new NodeFileCollection(
            context_datas['attachments']
        );
        this.collections.status_history = new StatusLogEntryCollection(
            context_datas.status_history
        );
        this.models.supplierOrder = new SupplierOrderModel(context_datas);
        this.setModelUrl('supplierOrder', AppOption['context_url']);
        this.computeLineTotal();
        this.computeFundingTotals();
    },
    computeLineTotal(){
        var collection = this.collections.lines;

        var datas = {};
        datas['ht'] = collection.total_ht();
        datas['tva'] = collection.total_tva();
        datas['ttc'] = collection.total();
        var channel = this.getChannel();
        channel.trigger('change:lines');
        this.models.total.set(datas);

        // Refresh funding totals as totals changed
        this.computeFundingTotals();
    },
    computeFundingTotals() {
        var order = this.models.supplierOrder;
        var datas = {};
        var ttc = this.models.total.get('ttc');
        var caePercentage = order.get('cae_percentage');

        datas['ttc_cae'] = getPercent(ttc, caePercentage);
        datas['ttc_worker'] = ttc - datas['ttc_cae'];
        this.models.total.set(datas);
    },
});
const Facade = new FacadeClass();
export default Facade;

import Mn from 'backbone.marionette';

import ImageViewerView from "../views/ImageViewerView";
import PDFViewerView from "../views/PDFViewerView";


/** Instantiate the propper viewer view for a NodeFileModel
 */
const NodeFileViewerFactoryClass = Mn.Object.extend({
    typesMap: {
        'image/jpeg': ImageViewerView,
        'image/svg+xml': ImageViewerView,
        'image/gif': ImageViewerView,
        'image/bitmap': ImageViewerView,
        'image/png': ImageViewerView,
        'image/svg': ImageViewerView,
        'image/webp': ImageViewerView,
        'image/apng': ImageViewerView,
        'image/tiff': ImageViewerView,
        'application/pdf': PDFViewerView,
    },
    /**
     *
     * @param {NodeFileModel} file the file object to be viewed
     * @param {Object} kwargs extra args to be passed as-is to viewer constructor
     * @returns {Mn.View} (or null if no viewer is found)
     */
    getViewer(file, kwargs) {
        const mime = file.get('mimetype');
        const id = file.get('id');
        const klass = this.typesMap[mime];

        if (klass) {
            const args = Object.assign(
                {
                    fileUrl: `/files/${id}?action=stream`,
                    fileLabel: file.get('label'),
                },
                kwargs,
            );
            return new klass(args);
        } else {
            console.warn(
                `Trying to render unknown file type: "${mime} for file #${id}`
            );
            return null;
        }
    },
});

let NodeFileViewerFactory = new NodeFileViewerFactoryClass();

export default NodeFileViewerFactory;

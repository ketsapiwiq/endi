import ToolbarAppClass from "./ToolbarAppClass";
import Radio from "backbone.radio";

const ValidationLimitToolbarAppClass = ToolbarAppClass.extend({
  initialize() {
    this.facade = Radio.channel('facade');
    this.listenTo(this.facade, 'changed:line', this.onLineUpdate);
    this.listenTo(this.facade, 'changed:task', this.onLineUpdate);
    this.listenTo(this.facade, 'changed:discount', this.onLineUpdate);
  },
  onLineUpdate() {
    this.config = Radio.channel('config');
    this.config.request('reload_config').then(() => {
      const actions = this.config.request('get:form_actions');
      this.renderApp(actions);
    })
  },
});
export default ValidationLimitToolbarAppClass;
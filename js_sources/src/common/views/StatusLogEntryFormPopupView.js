import Mn from 'backbone.marionette';

import TextAreaWidget from '../../widgets/TextAreaWidget.js';
import ModalFormBehavior from '../../base/behaviors/ModalFormBehavior.js';
import SelectWidget from "../../widgets/SelectWidget";
import Radio from "backbone.radio";
import CheckboxWidget from "../../widgets/CheckboxWidget";

const template = require('./templates/StatusLogEntryFormPopupView.mustache');

/** Popup to add freetext memo
 *
 * A single `description` field is split/unsplit from/to two fields on the model:
 * - label (first line)
 * - description (the remainder, if any).
 *
 * See splitDescriptionfield()/mergeDescriptionField()
 *
 * Requirements:
 * - a config channel answering to ('get:options', 'visibilities')
 */
const StatusLogEntryFormPopupView = Mn.View.extend({
    // Send the whole model on submit
    partial: false,
    behaviors: [ModalFormBehavior],
    template: template,
    regions: {
        'description': '.field_description',
        'visibility': '.field_visibility',
        'pinned': '.field_pinned',
    },
    ui: {
        form: 'form',
        submit: 'button[type=submit]',
    },
    // Listen to the current's view events
    events: {
        'click @ui.submit': 'onSubmit'
    },
    childViewEvents: {
        'finish:pinned': 'onChangePinned',
    },

    onChangePinned(pinnedValue){
        const pinnedCount = this.options.destCollection.getPinnedCount();
        if ((pinnedValue === true) && (pinnedCount >= 3)) {
            alert(
                `Il y déjà ${pinnedCount} mémos épinglés. `
                + "Tous ne pourront pas être affichés à la fois."
            );
        }
    },
    initialize(options){
      this.model = options.model;
      this.config = Radio.channel('config');
    },
    onRender(){
        this.showTextField();
        this.showVisibilityField();
        this.showPinnedField();
    },
    showTextField(){
        const view = new TextAreaWidget({
            label: "Texte",
            description: "La première ligne sera considérée comme le titre du mémo",
            field_name: 'description',
            value: this.mergeDescriptionField(
                this.model.get('label'),
                this.model.get('comment'),
            ),
            required: true,
            rows: 4,
        });
        this.showChildView('description', view);
    },
    showVisibilityField() {
        const view = new SelectWidget({
            options: this.config.request('get:options', 'visibilities'),
            title: "Visibilité",
            field_name: "visibility",
            value: this.model.get('visibility'),
            description: 'Les mémos perso restent visibles des administrateurs enDi',
        });
        this.showChildView('visibility', view);
    },
    showPinnedField() {
        const view = new CheckboxWidget({
            inline_label: "Épinglé",
            description: "Ce mémo sera épinglé en début de liste",
            true_val: true,
            false_val: false,
            field_name: "pinned",
            value: this.model.get('pinned'),
        });
        this.showChildView('pinned', view);
    },
    afterSerializeForm(datas) {
        _.extend(datas, this.splitDescriptionField(datas.description));
        delete datas['description']
        return datas;
    },
    splitDescriptionField(text) {
        const separator_position = text.search('\n');
        if (separator_position >= 0) {
            return {
                label: text.substring(0, separator_position).trim(),
                comment: text.substring(separator_position).trim(),
            };
        } else {
            return {label: text.trim(), comment: ''};
        }
    },
    mergeDescriptionField(label, comment){
        return `${label}\n\n${comment}`.trim();
    },
    templateContext(){
        const edit = this.getOption('edit');
        return {title: edit ? "Modifier ce mémo" : "Ajouter un mémo"};
    },
});
export default StatusLogEntryFormPopupView;

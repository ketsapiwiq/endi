import Mn from 'backbone.marionette';
import { formatDate } from '../../date.js';
import { addBr } from '../../string.js';

const StatusLogEntryItemView = Mn.View.extend({
    tagName: 'li',
    template: require('./templates/StatusLogEntryItemView.mustache'),
    ui: {
        'delete_btn': 'button.delete',
        'edit_btn': 'button.edit',
        'text_content': 'div',
    },
    events: {
        'click @ui.delete_btn': 'onDelete',
        'click @ui.edit_btn': 'onEdit',
        'click @ui.text_content': 'onTextContentClick',
    },
    modelEvents: {
        sync: 'render',
    },
    onDelete(event){
        event.stopPropagation();  // avoid triggering onTextContentClick
       if (window.confirm("Êtes-vous sûr de vouloir supprimer ce mémo ?")){
           this.model.destroy();
       }
    },
    onEdit(event){
        event.stopPropagation();   // avoid triggering onTextContentClick
        this.trigger('edit', this.model);
        // console.log('TRIGGER SORT');
        // this.collection.sort();
    },
    onTextContentClick(event, ){
        this.trigger('show_full', event, this.model);
    },
    getVisibilityContext() {
        const visibility = this.model.get('visibility');
        if (visibility === 'private') {
            return {
                label: "Perso",
                description: "visible uniquement par vous",
                icon: 'key',
            };
        } else if (visibility === 'management') {
            return {
                label: "Équipe",
                description: "visible uniquement par l'équipe d'appui",
                icon: 'users',
            };
        } else {
            // Public visibility is default and not explicitly marked
            return null;
        }
    },
    templateContext(){
        return {
            date: formatDate(this.model.get('datetime')),
            formatted_comment: addBr(this.model.get('comment')),
            visibility: this.getVisibilityContext(),
        };
    }
});

const StatusHistoryEmptyView = Mn.View.extend({
    tagName: 'li',
    template: require('./templates/StatusLogEntryEmptyView.mustache')
});

/**
 * @param {Number} limitTo max elements to display (0 or unset to show all)
*/
const StatusLogEntryCollectionView = Mn.CollectionView.extend({
    tagName: 'ul',
    childView: StatusLogEntryItemView,
    emptyView: StatusHistoryEmptyView,
    childViewTriggers: {
        'edit': 'statuslogentry:edit',
        'show_full': 'statuslogentry:show_full',
    },
    collectionEvents: {
        'sync': 'onChildSync',
    },
    events: {
      'render': 'onRender',
    },
    onChildSync(){
        // Pining may re-order childs
        this.collection.sort();
    },
    initialize(options){
        this.limitTo = options.limitTo;
        this.focusedModel = options.focusedModel;
    },
    viewFilter(child, index, collection){
        if (this.limitTo) {
            return index < this.limitTo;
        } else {
            return true;
        }
    },
    onAttach(){
        console.log('focusedModel = ', this.focusedModel);
        if (this.focusedModel) {
            const childView = this.children.findByModel(this.focusedModel);
            childView.$el[0].scrollIntoView(false);
            // Do it only once:
            this.focusedModel = undefined;
        }
    }
});
export default StatusLogEntryCollectionView;

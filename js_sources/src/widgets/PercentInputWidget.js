import Mn from 'backbone.marionette';
import { getOpt, setRangeSlider } from "../tools.js";
import InputWidget from "./InputWidget.js";


require('jquery');
import $ from 'jquery';


const PercentInputWidget = InputWidget.extend({
    tagName: 'div',
    className: 'form-group',
    template: require('./templates/PercentInputWidget.mustache'),
    ui: {
        input: 'input'
    },
    events: {
        'keyup @ui.input': 'onKeyUp',
        'input @ui.input': 'onKeyUp',
        'blur @ui.input': 'onBlur',
    },
    onInput: function () {
        // there is no keyup event for input type=range
        this.triggerChange(this.getUI('input').val());
    },
    onKeyUp: function(){
        this.triggerChange(this.getUI('input').val());
    },
    onBlur: function(){
        this.triggerFinish(this.getUI('input').val());
    },
});


export default PercentInputWidget;

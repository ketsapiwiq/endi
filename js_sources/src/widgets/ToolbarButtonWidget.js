import Mn from 'backbone.marionette';

/** A Widget for action links
 * Can be initialized in two ways :

    Passing a model with an 'option' attribute describing the Anchor

        new ToolbarButtonWidget({model: new Bb.Model({option: {href: 'https://endi.coop', label: 'Link to endi'}})});

    Passing directly the options on widget initialization

        new ToolbarButtonWidget({href: 'https://endi.coop', label: 'Link to endi'});
 *
 * It can handle GET actions and popup actions. If you need a POST action (eg:
 * non-idempotent action like deletion), have a look at POSTButtonWidget.
 *
 */
const ToolbarButtonWidget = Mn.View.extend({
    tagName: 'div',
    template: require('./templates/ToolbarButtonWidget.mustache'),
    ui: {
        button: 'button'
    },
    events: {
        'click @ui.button': "onClick",
    },
    findOptions(){
        let options;
        const model = this.getOption('model');
        if (model && model.has('option')){
            options = model.get('option');
        } else {
            options = this.options;
        }
        return options;
    },
    templateContext(){
        const options = this.findOptions();
        let onclick;
        if (! (options.onclick instanceof Function)){
            onclick = options.onclick;
        }
        return {
            option: this.findOptions(),
            onclick: onclick
        };
    },
    onClick(event) {
        const options = this.findOptions();
        if (options.popup){
            window.openPopup(options.url);
        } else if (options.onclick instanceof Function){
            options.onclick(event);
        }
    }
});


export default ToolbarButtonWidget;

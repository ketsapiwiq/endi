/*
 * File Name : BaseProductModel.js
 *
 */
import BaseModel from "base/models/BaseModel.js";
import Radio from 'backbone.radio';
import {
    formatAmount,
    strToFloat,
    round
} from 'math.js';
import DuplicableMixin from 'base/models/DuplicableMixin.js';
import StockOperationCollection from './StockOperationCollection.js';


const BaseProductModel = BaseModel.extend(DuplicableMixin).extend({
    props: [
        'id',
        'type_',
        'label',
        'description',
        'ht',
        'ttc',
        'unity',
        'tva_id',
        'product_id',
        'supplier_id',
        'supplier_ref',
        'supplier_unity_amount',
        'supplier_ht',
        'purchase_type_id',
        'margin_rate',
        'category_id',
        'category_label',
        'ref',

        'current_stock',
        'stock_operations',
        'notes',
        "locked",
        'mode',
    ],
    validation: {
        'label': {
            required: true,
            msg: "Veuillez saisir un nom"
        },
        ht: {
            required: false,
            pattern: "amount",
            msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule",
        },
        ttc: {
            required: false,
            pattern: "amount",
            msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule",
        },
        supplier_ht: {
            required: false,
            pattern: "amount",
            msg: "Veuillez saisir un coût d'achat, dans la limite de 5 chiffres après la virgule",
        },
        margin_rate: [{
                required: false,
                pattern: 'amount',
                msg: "Le coefficient de marge doit être un nombre entre 0 et 1, dans la limite de 5 chiffres après la virgule"
            },
            function (value) {
                value = parseInt(value.replace(',', '.'));
                if (value < 0 || value >= 1) {
                    return "Le coefficient de marge doit être un nombre entre 0 et 1"
                }

            }
        ],
    },
    icons: {
        sale_product_product: 'box',
        sale_product_material: 'box',
        sale_product_composite: 'product-composite',
        sale_product_training: 'chalkboard-teacher',
        sale_product_work_force: "user",
        sale_product_service_delivery: "hands-helping",
    },
    initialize: function () {
        BaseProductModel.__super__.initialize.apply(this, arguments);
        if ("collection" in arguments) {
            this.collection = arguments.collection;
        }
        this.facade = Radio.channel('facade');
        this.config = Radio.channel('config');
        this.tva_options = this.config.request(
            'get:options',
            'tvas'
        );
        this.supplier_options = this.config.request(
            'get:options',
            'suppliers'
        );
        this.is_complex = false;
        this.populate_stock_operations();
    },
    setupSyncEvents() {
        console.log("BaseProduct.onSetId");
        this.stopListening(this.stock_operations);
        this.listenTo(this.stock_operations, 'add', this.syncStockOperations);
        this.listenTo(this.stock_operations, 'sync', this.syncStockOperations);
        this.listenTo(this.stock_operations, 'remove', this.syncStockOperations);
        this.listenTo(this.stock_operations, 'change', this.syncStockOperations);
    },
    populate_stock_operations() {
        if (this.has('stock_operations')) {
            this.stock_operations = new StockOperationCollection(this.get('stock_operations'));
        } else {
            this.stock_operations = new StockOperationCollection([]);
        }

        const url = this.url();
        this.stock_operations.url = () => url + '/' + "stock_operations";
    },
    syncStockOperations() {
        if (!this.stock_operations) {
            this.populate_stock_operations();
        }
        this.set('stock_operations', this.stock_operations.toJSON());
    },
    category_label() {
        return this.get('category_label');
    },
    tva_label() {
        return this.findLabelFromId('tva_id', 'label', this.tva_options);
    },
    ht_label() {
        return formatAmount(this.get('ht'), true);
    },
    ttc_label() {
        return formatAmount(this.get('ttc'), true);
    },
    supplier_ht_label() {
        return formatAmount(this.get('supplier_ht'), false, false);
    },
    supplier_label() {
        return this.findLabelFromId('supplier_id', 'label', this.supplier_options);
    },
    matchPattern(search) {
        if (
            (this.get('label').indexOf(search) !== -1) ||
            (this.get('description').indexOf(search) !== -1) ||
            (this.category_label().indexOf(search) !== -1)
        ) {
            return true;
        }
        return false;
    },
    getIcon() {
        if (_.has(this.icons, this.get('type_'))) {
            return this.icons[this.get('type_')];
        } else {
            return false;
        }
    }
});
export default BaseProductModel;
/*
 * File Name :  StockOperationCollection
 */
import OrderableCollection from '../../base/models/OrderableCollection.js';
import StockOperationModel from './StockOperationModel.js';

const StockOperationCollection = OrderableCollection.extend({
    model: StockOperationModel,
});
export default StockOperationCollection;

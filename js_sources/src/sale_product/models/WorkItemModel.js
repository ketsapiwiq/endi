/*
 * File Name :  WorkItemModel
 */
import BaseModel from 'base/models/BaseModel.js';
import Radio from 'backbone.radio';
import {
    formatAmount
} from 'math.js';

const WorkItemModel = BaseModel.extend({
    props: [
        "id",
        "type_",
        "label",
        "description",
        "ht",
        "supplier_ht",
        "quantity",
        "unity",
        'base_sale_product_id',
        "sync_catalog",
        'locked',
        'mode',
        "total_ht",
    ],
    catalog_attributes: [
        'ht', 'supplier_ht', 'mode', 'unity'
    ],
    defaults() {
        return {
            quantity: 1,
            locked: true,
            mode: "supplier_ht"
        }
    },
    validation: {
        'label': {
            required: true,
            msg: "Veuillez saisir un nom"
        },
        description: {
            required: true,
            msg: "Veuillez saisir une description"
        },
        type_: {
            required: true,
            msg: "Veuillez choisir un type de produit"
        }
    },
    icons: {
        sale_product_product: 'box',
        sale_product_material: 'box',
        sale_product_composite: 'product-composite',
        sale_product_training: 'chalkboard-teacher',
        sale_product_work_force: "user",
        sale_product_service_delivery: "hands-helping",
    },
    initialize: function () {
        BaseModel.__super__.initialize.apply(this, arguments);
        this.facade = Radio.channel('facade');
    },
    supplier_ht_label() {
        if (this.get('mode') == 'supplier_ht') {
            return formatAmount(this.get('supplier_ht'), false, false);
        } else {
            return null;
        }
    },
    ht_label() {
        return formatAmount(this.get('ht'), false, false);
    },
    total_ht_label() {
        return formatAmount(this.get('total_ht'), false, false);
    },
    getIcon() {
        if (_.has(this.icons, this.get('type_'))) {
            return this.icons[this.get('type_')];
        } else {
            return false;
        }
    },
    isFromCatalog(attribute) {
        /*
         * Check if the given attribute comes from the catalog
         * Not editable
         */
        if (!this.get('id')) {
            // Add mode
            return false;
        }
        if (!this.get('locked')) {
            return false;
        }
        return this.catalog_attributes.indexOf(attribute) >= 0;
    }
});
export default WorkItemModel;
# ATTENTION : les fichiers docker ne sont actuellement maintenus par personne…
FROM debian:10

RUN apt-get update && apt-get install -y locales
RUN echo "fr_FR.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen
ENV LC_ALL fr_FR.UTF-8

RUN apt update && \
    apt install -y virtualenvwrapper libmariadbclient-dev libmariadb-dev-compat npm build-essential libjpeg-dev libfreetype6 libfreetype6-dev libssl-dev libxml2-dev zlib1g-dev python3-mysqldb libxslt1-dev python3-pip mariadb-client libcairo2 libpango-1.0.0 libpangocairo-1.0-0

RUN virtualenv /endi -p python3

COPY requirements.txt /endi-code/
WORKDIR /endi-code
RUN  /endi/bin/pip install -r requirements.txt

COPY . /endi-code

RUN /endi/bin/pip install -e /endi-code

COPY development.ini.docker development.ini

RUN npm --prefix js_sources install
RUN make prodjs devjs

CMD ["./docker_start.sh"]
EXPOSE 8080

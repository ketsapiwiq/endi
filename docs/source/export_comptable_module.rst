Modules d'export comptable
============================

enDI utilise pyramid_services pour accéder aux classes à utiliser lors du
processus d'export comptable.

Afin de produire des exports différents de ceux proposés par enDI, il est
possible de développer de nouvelles classes :

- Pour produire les données des écritures ( ITreasuryProducer )
- Pour exporter les écritures dans un fichier ( ITreasury<Model>Writer )


Exemple
........

Développer un Exporter Custom

.. code-block:: python

    from endi.compute.sage.invoice import SageFacturation

    class MainInvoice(SageFacturation):
        """
        """
        static_columns = (
            'code_journal',
            'date',
            'num_facture',
            'libelle',
            'type_',
            'currency',
        )

        @property
        def libelle(self):
            return self.invoice.customer.name.upper()

        @property
        def date(self):
            """
                Return the date field
            """
            return self.invoice.date

        @property
        def currency(self):
            return "EUR"


Utilisez cet exporter dans un Producer

.. code-block:: python

	from endi.compute.sage.invoice import InvoiceExportProducer

	class InvoiceProducer(InvoiceExportProducer):
		"""
		Custom producer pour les factures
		c'est lui qui produit les lignes d'export pour les factures
		ici il s'assure de vire une des deux lignes analytiques/générales
		"""
		use_analytic = False
		_default_modules = (MainInvoice, )
		_available_modules = {}


Ajouter un Writer spécifique

.. code-block:: python

	from sqla_inspect.excel import XlsExporter
	from endi_base.utils.date import format_short_date
	from endi.utils import strings

	class InvoiceWriter(XlsExporter):
		"""
		Sage invoice csv writer
		Add the handling of the invoice prefix in invoice number formatting
		"""
		encoding = "utf-8"
		mimetype = (
			"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
		)
		extension = 'xlsx'
		amount_precision = 5
		headers = (
			{'name': 'date', 'label': "date", "typ": "date"},
			{'name': 'num_facture', 'label': 'n° de pièce'},
			{'name': 'compte_cg', 'label': "n° de compte"},
			{'name': 'libelle', 'label': 'libellé'},
			{"name": "currency", "label": "Colonne vide"},
			{'name': 'debit', 'label': "débit", "typ": "number"},
			{'name': 'credit', 'label': "crédit", "typ": "number"},
		)

		def __init__(self, *args):
			XlsExporter.__init__(self)

		def format_debit(self, debit):
			"""
				Format the debit entry to get a clean float in our export
				12000 => 120,00
			"""
			if debit == '':
				return 0
			else:
				return strings.format_amount(
					debit,
					grouping=False,
					precision=self.amount_precision
				).replace(',', '.')

		def format_credit(self, credit):
			"""
				format the credit entry to get a clean float
			"""
			return self.format_debit(credit)

		def format_num_facture(self, value):
			return "F{0}".format(value)

		def format_date(self, value):
			return format_short_date(value)


Configurer enDI dans le fichier .ini pour utiliser ces éléments

.. code-block::

	[app:endi]
	...
	endi.services.treasury_invoice_writer=endi.export.custom.InvoiceWriter
	endi.services.treasury_invoice_producer=endi.export.custom.InvoiceProducer
	...


Les écritures se feront alors au format xlsx, les données seront reformattées
selon les règles ci-dessus.

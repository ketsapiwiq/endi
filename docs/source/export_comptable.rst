Export comptables des documents et paiements
=============================================

Les vues sont dans endi/views/export/

Les outils de production d'écritures sont dans endi/compute/sage

Les outils d'export vers des fichiers sont dans endi/export/sage.py

Les interfaces utilisées pour plugger des modules d'exports sont dans endi/interfaces.py


Important :

Les formats de données et le format des fichiers de sortis sont dépendant des ExportProducer et ExportWriter qui sont configurés dans le fichier .ini.

Par exemple les écritures produites par défaut contiennent une ligne générale et une ligne analytique, ce n'est pas forcément le cas.

De même les fichiers de sorties ne sont pas forcément en csv et ne peuvent contenir des entêtes différentes de celles par défaut.

.. include:: export_comptable_module.rst


Le schéma ci-dessous décrit le fonctionnement de l'export



.. image:: _static/export_comptable/cycle_export_comptable.png
  :width: 100%
  :alt: Diagramme décrivant le fonctionnement des exports

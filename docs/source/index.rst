.. enDI documentation master file, created by
   sphinx-quickstart on Fri Jan 25 10:44:18 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

enDI
=========

Contents:

.. toctree::
    :maxdepth: 2

    lexique
    documents
    expense
    ftp
    hacking
    decoupage
    js_templates
    javascript
    formulaire_javascript
    sqlalchemy_index
    rest
    computing
    apidoc/modules
    classes
    styleguide
    pyramid_traversal
    celery
    service
    tests
    endisap
    endisolo
    export_comptable

.. include:: components.rst

.. include:: views.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


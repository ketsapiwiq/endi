endi.panels package
========================

Submodules
----------

endi.panels.company module
-------------------------------

.. automodule:: endi.panels.company
    :members:
    :undoc-members:
    :show-inheritance:

endi.panels.invoicetable module
------------------------------------

.. automodule:: endi.panels.invoicetable
    :members:
    :undoc-members:
    :show-inheritance:

endi.panels.menu module
----------------------------

.. automodule:: endi.panels.menu
    :members:
    :undoc-members:
    :show-inheritance:

endi.panels.task module
----------------------------

.. automodule:: endi.panels.task
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: endi.panels
    :members:
    :undoc-members:
    :show-inheritance:

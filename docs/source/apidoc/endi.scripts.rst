endi.scripts package
=========================

Submodules
----------

endi.scripts.add_admin module
----------------------------------

.. automodule:: endi.scripts.add_admin
    :members:
    :undoc-members:
    :show-inheritance:

endi.scripts.fake_database module
--------------------------------------

.. automodule:: endi.scripts.fake_database
    :members:
    :undoc-members:
    :show-inheritance:

endi.scripts.migrate module
--------------------------------

.. automodule:: endi.scripts.migrate
    :members:
    :undoc-members:
    :show-inheritance:

endi.scripts.utils module
------------------------------

.. automodule:: endi.scripts.utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: endi.scripts
    :members:
    :undoc-members:
    :show-inheritance:

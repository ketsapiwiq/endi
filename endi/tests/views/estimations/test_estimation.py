import pytest
from endi.views.estimations.estimation import (
    EstimationAddView,
    estimation_geninv_view,
    EstimationDuplicateView,
)
from endi.models.task import Estimation
from endi.views.project.routes import PROJECT_ITEM_ESTIMATION_ROUTE


class TestAddView:
    @pytest.fixture
    def view_result(
        self,
        get_csrf_request_with_db,
        company,
        project,
        customer,
        default_business_type,
    ):
        def callview(params={}):
            props = {}
            props.update(
                {
                    "name": "Devis 1",
                    "submit": "deform",
                    "company_id": str(company.id),
                    "customer_id": str(customer.id),
                    "project_id": str(project.id),
                    "business_type_id": str(default_business_type.id),
                }
            )
            props.update(params)
            project.__name__ = "project"
            request = get_csrf_request_with_db(
                post=props,
                current_route_path=PROJECT_ITEM_ESTIMATION_ROUTE,
                context=project,
            )

            view = EstimationAddView(project, request)
            view.__call__()
            return Estimation.query().all()[-1]

        return callview

    def test_add(self, view_result, company, project, customer):
        estimation = view_result()
        assert isinstance(estimation, Estimation)
        assert len(estimation.payment_lines) == 1
        assert estimation.project == project
        assert estimation.company == company
        assert estimation.customer == customer
        assert not estimation.has_price_study()

    def test_add_restore_previous_true(self, plugin_active, view_result, mk_estimation):
        mk_estimation(display_ttc=1, display_units=1)
        estimation = view_result()
        if not plugin_active("sap"):
            assert estimation.display_ttc == 1
            assert estimation.display_units == 1

    def test_add_restore_previous_false(
        self, plugin_active, view_result, mk_estimation
    ):
        mk_estimation(display_ttc=0, display_units=0)
        estimation = view_result()
        if not plugin_active("sap"):
            assert estimation.display_ttc == 0
            assert estimation.display_units == 0
        else:
            assert estimation.display_ttc
            assert estimation.display_units

    def test_add_price_study(self, view_result, mk_project, mk_project_type):
        tp = mk_project_type(
            "test", include_price_study=True, price_study_mode="optionnal"
        )
        p = mk_project(project_type=tp)
        estimation = view_result(
            {
                "project_id": str(p.id),
            }
        )
        assert not estimation.has_price_study()
        tp.price_study_mode = "default"
        estimation = view_result({"project_id": str(p.id)})
        assert estimation.has_price_study()
        tp.price_study_mode = "mandatory"
        estimation = view_result({"project_id": str(p.id)})
        assert estimation.has_price_study()


def test_geninv_view(config, get_csrf_request_with_db, full_estimation):
    config.add_route("/invoices/{id}", "/invoices/{id}")
    estimation_geninv_view(full_estimation, get_csrf_request_with_db())
    assert full_estimation.geninv
    assert len(full_estimation.invoices) > 0
    for inv in full_estimation.invoices:
        assert inv.business_id == full_estimation.business_id


class TestEstimationDuplicate:
    def duplicate_one(self, config, request, customer, project, full_estimation):
        config.add_route("/estimations/{id}", "/")
        params = {
            "customer_id": customer.id,
            "project_id": project.id,
            "business_type_id": full_estimation.business_type_id,
        }
        request.context = full_estimation
        view = EstimationDuplicateView(request)
        view.submit_success(params)

    def get_one(self):
        return Estimation.query().order_by(Estimation.id.desc()).first()

    def test_duplicate(
        self,
        config,
        get_csrf_request_with_db_and_user,
        mk_customer,
        mk_project,
        full_estimation,
    ):
        new_customer = mk_customer(name="newcustomer")
        new_project = mk_project(name="newproject", customers=[new_customer])
        request = get_csrf_request_with_db_and_user()
        self.duplicate_one(config, request, new_customer, new_project, full_estimation)
        estimation = self.get_one()
        assert estimation.type_ == "estimation"
        assert estimation.customer_id == new_customer.id
        assert estimation.project_id == new_project.id

    def test_duplicate_internal(
        self,
        config,
        get_csrf_request_with_db_and_user,
        mk_customer,
        mk_project,
        full_estimation,
        mk_tva,
        mk_product,
    ):
        new_tva = mk_tva(value=0, name="O%")
        new_product = mk_product(tva=new_tva, name="interne", internal=True)

        new_customer = mk_customer(name="newcustomer", type="internal")
        new_project = mk_project(name="newproject", customers=[new_customer])
        request = get_csrf_request_with_db_and_user()

        self.duplicate_one(config, request, new_customer, new_project, full_estimation)
        estimation = self.get_one()
        assert estimation.type_ == "internalestimation"
        assert estimation.customer_id == new_customer.id
        assert estimation.project_id == new_project.id
        for line in estimation.all_lines:
            assert line.tva == new_tva.value
            assert line.product_id == new_product.id

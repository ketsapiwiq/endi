import pytest
from endi.utils.rest import RestError
from endi.views.third_party.customer.rest_api import CustomerRestView


class TestCustomerRestView:
    def test_get_customers(
        self,
        get_csrf_request_with_db,
        customer,
        company,
    ):
        request = get_csrf_request_with_db()
        request.context = company
        view = CustomerRestView(request)
        result = view.collection_get()
        assert len(result) == 1
        assert result[0].name == customer.name

    def test_get_customers_other_company(
        self,
        dbsession,
        get_csrf_request_with_db,
        customer,
        company2,
    ):
        request = get_csrf_request_with_db()
        request.context = company2
        view = CustomerRestView(request)
        result = view.collection_get()
        assert len(result) == 0

    def test_post_company(self, get_csrf_request_with_db, company):
        appstruct = {"company_name": "Entreprise", "__formid__": "company"}
        request = get_csrf_request_with_db(post=appstruct)
        request.context = company
        view = CustomerRestView(request)
        view.post()

        customer = company.customers[0]
        assert customer.company_name == "Entreprise"
        assert customer.type == "company"

    def test_post_individual(self, get_csrf_request_with_db, company):
        appstruct = {"company_name": "Entreprise", "__formid__": "individual"}
        request = get_csrf_request_with_db(post=appstruct)
        request.context = company
        view = CustomerRestView(request)
        with pytest.raises(RestError):
            view.post()

        appstruct = {
            "lastname": "Dupont",
            "firstname": "Jean",
            "__formid__": "individual",
        }
        request = get_csrf_request_with_db(post=appstruct)
        view = CustomerRestView(request)
        view.post()
        customer = company.customers[0]
        assert customer.lastname == "Dupont"
        assert customer.firstname == "Jean"
        assert customer.type == "individual"

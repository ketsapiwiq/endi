import colander

from endi.models.third_party.customer import Customer
from endi.views.third_party.customer.views import (
    CustomerAddView,
    CustomerEditView,
    customer_delete,
    customer_archive,
)


COMPANY_APPSTRUCT = {
    "__formid__": "company",
    "company_name": "Company",
    "lastname": "Lastname",
    "address": "Address",
    "zip_code": "21000",
    "city": "Dijon",
    "compte_cg": "Compte CG1515",
    "compte_tiers": "Compte Tiers",
    "code": "CODE",
}


INDIVIDUAL_APPSTRUCT = {
    "__formid__": "individual",
    "lastname": "Lastname 2",
    "firstname": "FirstName",
    "address": "Address",
    "zip_code": "21000",
    "city": "Dijon",
    "compte_cg": "Compte CG1515",
    "compte_tiers": "Compte Tiers",
    "code": "CODE",
}


def get_company_customer():
    return Customer.query().filter(Customer.company_name == "Company").one()


def get_individual_customer():
    return Customer.query().filter(Customer.lastname == "Lastname 2").one()


class TestCustomerAddView:
    def test_get_current_form_name(self, get_csrf_request_with_db, company):
        pyramid_request = get_csrf_request_with_db(
            post={"__formid__": "individual"}, context=company
        )
        view = CustomerAddView(pyramid_request)
        assert view.get_current_form_name() == "individual"

    def test_schema(self, get_csrf_request_with_db, company):
        pyramid_request = get_csrf_request_with_db(
            post={"__formid__": "company"}, context=company
        )
        view = CustomerAddView(pyramid_request)
        schema = view.get_current_form().schema
        assert schema["company_name"].missing == colander.required

    def test_submit_company_success(self, config, get_csrf_request_with_db, company):
        config.add_route("customer", "/{id}")
        pyramid_request = get_csrf_request_with_db(
            post=COMPANY_APPSTRUCT, context=company
        )
        view = CustomerAddView(pyramid_request)
        view._handle_submit()
        customer = get_company_customer()
        for key, value in COMPANY_APPSTRUCT.items():
            if hasattr(customer, key):
                assert getattr(customer, key) == value
        assert customer.type == "company"
        assert customer.company == company

    def test_submit_individual_success(self, config, get_csrf_request_with_db, company):
        config.add_route("customer", "/")
        pyramid_request = get_csrf_request_with_db(
            post=INDIVIDUAL_APPSTRUCT, context=company
        )

        view = CustomerAddView(pyramid_request)
        view._handle_submit()
        customer = get_individual_customer()
        for key, value in INDIVIDUAL_APPSTRUCT.items():
            if hasattr(customer, key):
                assert getattr(customer, key) == value
        assert customer.type == "individual"
        assert customer.company == company
        assert customer.label == customer._get_label()


class TestCustomerEditView:
    def test_customer_edit(self, config, customer, get_csrf_request_with_db):
        config.add_route("customer", "/")
        appstruct = COMPANY_APPSTRUCT.copy()
        appstruct["lastname"] = "Changed Lastname"
        appstruct["compte_cg"] = "1"
        appstruct["compte_tiers"] = "2"
        appstruct["submit"] = "1"
        req = get_csrf_request_with_db(post=appstruct, context=customer)
        view = CustomerEditView(req)
        view()
        assert customer.lastname == "Changed Lastname"
        assert customer.compte_cg == "1"
        assert customer.compte_tiers == "2"
        assert customer.label == customer._get_label()


def test_customer_delete(customer, get_csrf_request_with_db):
    req = get_csrf_request_with_db()
    cid = customer.id
    req.context = customer
    req.referer = "/"
    customer_delete(req)
    req.dbsession.flush()
    assert Customer.get(cid) is None


def test_customer_archive(customer, get_csrf_request_with_db):
    req = get_csrf_request_with_db()
    cid = customer.id
    req.context = customer
    req.referer = "/"
    customer_archive(req)
    req.dbsession.flush()
    assert Customer.get(cid).archived
    customer_archive(req)
    req.dbsession.flush()
    assert Customer.get(cid).archived is False

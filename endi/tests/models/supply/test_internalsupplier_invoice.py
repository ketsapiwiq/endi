from endi.models.supply.internalsupplier_invoice import InternalSupplierInvoice


def test_internasupplier_invoice_from_invoice(internalinvoice, supplier):
    internalinvoice.official_number = "4242"
    supplier_invoice = InternalSupplierInvoice.from_invoice(internalinvoice, supplier)

    assert supplier_invoice.remote_invoice_number == "4242"
    assert supplier_invoice.date == internalinvoice.date
    assert len(supplier_invoice.lines) == 1

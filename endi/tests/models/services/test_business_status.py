import datetime
from endi.models.services.business_status import BusinessStatusService


def test_populate_indicators(business):
    from endi.models.indicators import CustomBusinessIndicator

    def _indicator():
        return (
            CustomBusinessIndicator.query()
            .filter_by(
                name="bpf_filled",
                business=business,
            )
            .first()
        )

    BusinessStatusService.populate_indicators(business)
    indicator = business.indicators[0]
    assert indicator.label == "Facturation"
    assert indicator.status == "danger"
    assert indicator.name == "invoiced"


def test_update_classic_invoicing_indicator(business, invoice):
    from endi.models.services.business import BusinessService

    BusinessService.populate_deadlines(business)
    BusinessStatusService.populate_indicators(business)

    BusinessStatusService.update_classic_invoicing_indicator(business)
    assert business.indicators[0].status == "danger"

    for deadline in business.payment_deadlines:
        deadline.invoiced = True

    BusinessStatusService.update_classic_invoicing_indicator(business)
    assert business.indicators[0].status == "success"

    # Nouveau devis dans une affaire facturée
    business.payment_deadlines[0].invoiced = False
    BusinessStatusService.update_classic_invoicing_indicator(business)
    assert business.indicators[0].status == "danger"


def test_update_invoicing_status_classic(dbsession, business, invoice):
    from endi.models.services.business import BusinessService

    BusinessService.populate_deadlines(business)

    deadline = business.payment_deadlines[0]
    deadline.invoice = invoice
    dbsession.merge(deadline)
    dbsession.flush()
    BusinessStatusService.update_invoicing_status(business, invoice)
    assert deadline.invoiced is True


# def test_update_invoicing_status_progress_invoicing(
#     dbsession,
#     business_with_progress_invoicing,
# ):
#     business = business_with_progress_invoicing
#     for group_status in business.progress_invoicing_group_statuses:
#         for line_status in group_status.line_statuses:
#             line_status.percent_left = 0
#             dbsession.merge(line_status)
#             dbsession.flush()
#     BusinessStatusService.update_invoicing_status(business)
#     assert business.invoiced
#     for group_status in business.progress_invoicing_group_statuses:
#         for line_status in group_status.line_statuses:
#             line_status.percent_left = 5
#             dbsession.merge(line_status)
#             dbsession.flush()
#     BusinessStatusService.update_invoicing_status(business)
#     assert not business.invoiced


def test_compute_status(business):
    BusinessStatusService.populate_indicators(business)
    assert BusinessStatusService._compute_status(business) == "danger"

    for indicator in business.indicators:
        indicator.status = indicator.SUCCESS_STATUS

    assert BusinessStatusService._compute_status(business) == "success"


def test_update_status(business):
    BusinessStatusService.populate_indicators(business)

    for indicator in business.indicators:
        indicator.status = indicator.SUCCESS_STATUS

    BusinessStatusService.update_status(business) == "success"
    assert business.status == "success"


def test_create_bpf_status_indicator(training_business):
    from endi.models.indicators import CustomBusinessIndicator

    BusinessStatusService.populate_indicators(training_business)
    indicators = CustomBusinessIndicator.query().filter_by(
        name="bpf_filled",
        business=training_business,
    )
    assert indicators.count() == 1


def test_create_bpf_status_indicator_issue_1743(business):
    from endi.models.indicators import CustomBusinessIndicator

    BusinessStatusService.populate_indicators(business)
    indicators = CustomBusinessIndicator.query().filter_by(
        name="bpf_filled",
        business=business,
    )
    assert indicators.count() == 0
    assert BusinessStatusService.get_or_create_bpf_indicator(business) is None


def test_update_bpf_indicator_issue_1743(business):
    assert BusinessStatusService.update_bpf_indicator(business) is None


def test_update_bpf_status_indicator_single_year(
    training_business, mk_invoice, mk_business_bpf_data
):
    from endi.models.indicators import CustomBusinessIndicator

    BusinessStatusService.populate_indicators(training_business)
    BusinessStatusService.update_bpf_indicator(training_business)

    def _indicator():
        return (
            CustomBusinessIndicator.query()
            .filter_by(
                name="bpf_filled",
                business=training_business,
            )
            .first()
        )

    assert _indicator().status == CustomBusinessIndicator.WARNING_STATUS

    mk_invoice(
        business=training_business,
        financial_year=2019,
    )

    # Warn for past year missing BPF
    BusinessStatusService.update_bpf_indicator(training_business, current_year=2019)
    assert _indicator().status == CustomBusinessIndicator.WARNING_STATUS

    # Alert for past year missing BPF
    BusinessStatusService.update_bpf_indicator(training_business, current_year=2020)
    assert _indicator().status == CustomBusinessIndicator.DANGER_STATUS

    # No matter when we are, we have matching invoice and bpf, status must be
    # valid
    mk_business_bpf_data(business=training_business, financial_year=2019)
    BusinessStatusService.update_bpf_indicator(training_business, current_year=2020)
    assert _indicator().status == CustomBusinessIndicator.SUCCESS_STATUS
    BusinessStatusService.update_bpf_indicator(training_business, current_year=2019)
    assert _indicator().status == CustomBusinessIndicator.SUCCESS_STATUS


def test_update_bpf_status_indicator_multi_year(
    training_business,
    mk_invoice,
    mk_business_bpf_data,
    dbsession,
):
    from endi.models.indicators import CustomBusinessIndicator

    def _indicator():
        return (
            CustomBusinessIndicator.query()
            .filter_by(
                name="bpf_filled",
                business=training_business,
            )
            .first()
        )

    # Oldie
    mk_invoice(
        business=training_business,
        financial_year=2018,
    )
    # Past year
    mk_invoice(
        business=training_business,
        financial_year=2019,
    )
    # Current year
    mk_invoice(
        business=training_business,
        financial_year=2020,
    )

    # With no BPF : DANGER (triggered by past year invoice)
    BusinessStatusService.update_bpf_indicator(training_business, current_year=2020)
    assert _indicator().status == CustomBusinessIndicator.DANGER_STATUS

    # With filled past year, but nothing for others
    mk_business_bpf_data(business=training_business, financial_year=2019)
    BusinessStatusService.update_bpf_indicator(training_business, current_year=2020)
    assert _indicator().status == CustomBusinessIndicator.WARNING_STATUS

    # With only oldie not filled
    current_bpf = mk_business_bpf_data(business=training_business, financial_year=2020)
    BusinessStatusService.update_bpf_indicator(training_business, current_year=2020)
    assert _indicator().status == CustomBusinessIndicator.WARNING_STATUS

    # With everything filled
    mk_business_bpf_data(business=training_business, financial_year=2018)
    BusinessStatusService.update_bpf_indicator(training_business, current_year=2020)
    assert _indicator().status == CustomBusinessIndicator.SUCCESS_STATUS

    # With only current year not filled
    dbsession.delete(current_bpf)
    dbsession.flush()
    dbsession.refresh(training_business)
    BusinessStatusService.update_bpf_indicator(training_business, current_year=2020)
    assert _indicator().status == CustomBusinessIndicator.WARNING_STATUS

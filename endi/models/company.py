"""
    Company model
"""
import logging

from sqlalchemy import (
    Table,
    Column,
    Integer,
    Numeric,
    String,
    Text,
    ForeignKey,
    Date,
    Boolean,
)
from sqlalchemy.orm import (
    relationship,
    deferred,
    backref,
)

from endi_base.models.base import (
    DBBASE,
    DBSESSION,
    default_table_args,
)

from endi.compute import math_utils
from endi.models.node import Node
from endi.models.config import Config
from endi.models.options import (
    ConfigurableOption,
    get_id_foreignkey_col,
)
from endi.models.tools import (
    get_excluded_colanderalchemy,
)
from endi.models.services.company import CompanyService
from endi.models.user.user import COMPANY_EMPLOYEE

log = logging.getLogger(__name__)


COMPANY_ACTIVITY = Table(
    "company_activity_rel",
    DBBASE.metadata,
    Column(
        "company_id",
        ForeignKey("company.id", ondelete="CASCADE"),
    ),
    Column("activity_id", ForeignKey("company_activity.id", ondelete="CASCADE")),
    mysql_charset=default_table_args["mysql_charset"],
    mysql_engine=default_table_args["mysql_engine"],
)


class CompanyActivity(ConfigurableOption):
    """
    Company activities
    """

    __colanderalchemy_config__ = {
        "title": "Domaine d'activité",
        "validation_msg": "Les domaines d'activité ont bien été configurées",
    }
    id = get_id_foreignkey_col("configurable_option.id")


class Company(Node):
    """
    Company model
    Store all company specific stuff (headers, logos, RIB, ...)
    """

    __tablename__ = "company"
    __table_args__ = default_table_args
    __mapper_args__ = {"polymorphic_identity": "company"}
    # Meta informations
    id = Column(
        ForeignKey("node.id"),
        primary_key=True,
        info={"colanderalchemy": {"exclude": True}},
    )

    active = deferred(Column(Boolean(), default=True))

    # Informations générales
    # FIXME : pas utilisé
    goal = deferred(
        Column(
            "object",
            String(255),
            default="",
            info={
                "colanderalchemy": {
                    "title": "Descriptif de l'activité",
                    "section": "Informations publiques",
                },
            },
        ),
        group="edit",
    )
    email = deferred(
        Column(
            "email",
            String(255),
            info=dict(
                colanderalchemy=dict(title="E-mail", section="Informations publiques")
            ),
        ),
        group="edit",
    )
    phone = deferred(
        Column(
            "phone",
            String(20),
            default="",
            info=dict(
                colanderalchemy=dict(
                    title="Téléphone", section="Informations publiques"
                )
            ),
        ),
        group="edit",
    )
    mobile = deferred(
        Column(
            "mobile",
            String(20),
            info=dict(
                colanderalchemy=dict(
                    title="Téléphone mobile", section="Informations publiques"
                )
            ),
        ),
        group="edit",
    )
    address = deferred(
        Column(
            "address",
            String(255),
            info={
                "colanderalchemy": {
                    "title": "Adresse",
                    "section": "Informations publiques",
                }
            },
            default="",
        ),
        group="edit",
    )
    zip_code = deferred(
        Column(
            "zip_code",
            String(20),
            info={
                "colanderalchemy": {
                    "title": "Code postal",
                    "section": "Informations publiques",
                },
            },
            default="",
        ),
        group="edit",
    )
    city = deferred(
        Column(
            "city",
            String(255),
            info={
                "colanderalchemy": {
                    "title": "Ville",
                    "section": "Informations publiques",
                }
            },
            default="",
        ),
        group="edit",
    )
    country = deferred(
        Column(
            "country",
            String(150),
            info={
                "colanderalchemy": {
                    "title": "Pays",
                    "section": "Informations publiques",
                },
            },
            default="France",
        ),
        group="edit",
    )
    # Personnalisation des documents
    decimal_to_display = deferred(
        Column(
            Integer,
            default=2,
            info={
                "colanderalchemy": {
                    "title": "Nombre de décimales à afficher dans les sous-totaux",
                    "description": (
                        "Pour les prix unitaires et les sous-totaux HT, "
                        "Indiquez le nombre de décimales à afficher"
                    ),
                    "section": "Personnalisation des documents",
                }
            },
        ),
        group="edit",
    )

    logo_id = Column(
        ForeignKey("file.id"),
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )

    header_id = Column(
        ForeignKey("file.id"),
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )
    cgv = deferred(
        Column(
            Text,
            default="",
            info=dict(
                colanderalchemy=dict(
                    title="Conditions générales complémentaires",
                    section="Personnalisation des documents",
                ),
            ),
        ),
        group="edit",
    )
    # Paramètres techniques
    internal = Column(
        Boolean(),
        default=False,
        nullable=False,
        info=dict(
            colanderalchemy=dict(
                title="Interne",
                label="Enseigne interne à la CAE",
                section="Paramètres techniques (compta, gestion)",
                description="""
            À cocher si l'enseigne est utilisé pour abriter l'activité
            interne à la CAE, par opposition avec l'activité des entrepreneurs.
            Vous pouvez aussi configurer les enseignes internes dans
            Configuration → Configuration Générale →
            Enseigne(s) interne(s) à la CAE.
        """,
            ),
        ),
    )
    RIB = deferred(
        Column(
            "RIB",
            String(255),
            info=dict(
                colanderalchemy=dict(
                    title="RIB",
                    section="Paramètres techniques (compta, gestion)",
                )
            ),
        ),
        group="edit",
    )
    IBAN = deferred(
        Column(
            "IBAN",
            String(255),
            info=dict(
                colanderalchemy=dict(
                    title="IBAN",
                    section="Paramètres techniques (compta, gestion)",
                )
            ),
        ),
        group="edit",
    )

    code_compta = deferred(
        Column(
            String(30),
            default="",
            info=dict(
                colanderalchemy=dict(
                    title="Compte analytique",
                    description=(
                        "Compte analytique utilisé dans le logiciel de comptabilité"
                    ),
                    section="Paramètres techniques (compta, gestion)",
                ),
            ),
        ),
        group="edit",
    )

    general_customer_account = deferred(
        Column(
            String(255),
            default="",
            info=dict(
                colanderalchemy=dict(
                    title="Compte client général",
                    description=(
                        "Laisser vide pour utiliser les paramètres de la "
                        "configuration générale"
                        ""
                    ),
                    section="Paramètres techniques (compta, gestion)",
                )
            ),
        ),
        group="edit",
    )

    general_expense_account = deferred(
        Column(
            String(255),
            default="",
            info=dict(
                colanderalchemy=dict(
                    section="Paramètres techniques (compta, gestion)",
                    title=(
                        "Compte général (classe 4) pour les dépenses dues aux"
                        " entrepreneurs"
                    ),
                    description=(
                        "Concerne notes de dépense et l'éventuelle part"
                        " entrepreneur des factures fournisseur. Laisser vide"
                        " pour utiliser les paramètres de la configuration"
                        " générale"
                    ),
                )
            ),
        ),
        group="edit",
    )

    third_party_customer_account = deferred(
        Column(
            String(255),
            default="",
            info=dict(
                colanderalchemy=dict(
                    title="Compte client tiers",
                    description=(
                        "Laisser vide pour utiliser les paramètres de la "
                        "configuration générale"
                    ),
                    section="Paramètres techniques (compta, gestion)",
                )
            ),
        ),
        group="edit",
    )

    general_supplier_account = deferred(
        Column(
            String(255),
            default="",
            info=dict(
                colanderalchemy=dict(
                    title="Compte fournisseur général",
                    description=(
                        "Laisser vide pour utiliser les paramètres de la "
                        "configuration générale"
                    ),
                    section="Paramètres techniques (compta, gestion)",
                )
            ),
        ),
        group="edit",
    )

    third_party_supplier_account = deferred(
        Column(
            String(255),
            default="",
            info=dict(
                colanderalchemy=dict(
                    title="Compte fournisseur tiers",
                    description=(
                        "Laisser vide pour utiliser les paramètres de la "
                        "configuration générale"
                    ),
                    section="Paramètres techniques (compta, gestion)",
                )
            ),
        ),
        group="edit",
    )
    internalgeneral_customer_account = deferred(
        Column(
            String(255),
            default="",
            info=dict(
                colanderalchemy=dict(
                    title="Compte client général pour les clients internes",
                    description=(
                        "Laisser vide pour utiliser les paramètres de la "
                        "configuration générale"
                        ""
                    ),
                    section="Paramètres techniques (compta, gestion)",
                )
            ),
        ),
        group="edit",
    )

    internalthird_party_customer_account = deferred(
        Column(
            String(255),
            default="",
            info=dict(
                colanderalchemy=dict(
                    title="Compte client tiers pour les clients internes",
                    description=(
                        "Laisser vide pour utiliser les paramètres de la "
                        "configuration générale"
                    ),
                    section="Paramètres techniques (compta, gestion)",
                )
            ),
        ),
        group="edit",
    )

    internalgeneral_supplier_account = deferred(
        Column(
            String(255),
            default="",
            info=dict(
                colanderalchemy=dict(
                    title="Compte fournisseur général pour les fournisseurs internes",
                    description=(
                        "Laisser vide pour utiliser les paramètres de la "
                        "configuration générale"
                    ),
                    section="Paramètres techniques (compta, gestion)",
                ),
            ),
        ),
        group="edit",
    )

    internalthird_party_supplier_account = deferred(
        Column(
            String(255),
            default="",
            info=dict(
                colanderalchemy=dict(
                    title="Compte fournisseur tiers pour les fournisseurs internes",
                    description=(
                        "Laisser vide pour utiliser les paramètres de la "
                        "configuration générale"
                    ),
                    section="Paramètres techniques (compta, gestion)",
                ),
            ),
        ),
        group="edit",
    )

    # FIXME : pas sûr que cet attribut soit utilisé, à investiguer
    bank_account = deferred(
        Column(
            String(255),
            default="",
        ),
        group="edit",
    )
    # Taux de contribution et d'assurance "custom" par enseigne
    insurance = deferred(
        Column(
            Numeric(5, 2, asdecimal=False),
            info=dict(
                colanderalchemy=dict(
                    title="Taux d'assurance professionnelle",
                    description="Taux d'assurance à utiliser pour cette enseigne",
                    section="Paramètres techniques (compta, gestion)",
                ),
            ),
        ),
        group="edit",
    )
    internalinsurance = deferred(
        Column(
            Numeric(5, 2, asdecimal=False),
            info=dict(
                colanderalchemy=dict(
                    title=(
                        "Taux d'assurance professionnelle pour la facturation interne"
                    ),
                    description=(
                        "Taux d'assurance à utiliser pour cette enseigne"
                        " lorsqu'elle facture en interne"
                    ),
                    section="Paramètres techniques (compta, gestion)",
                ),
            ),
        ),
        group="edit",
    )
    contribution = deferred(
        Column(
            Numeric(5, 2, asdecimal=False),
            info=dict(
                colanderalchemy=dict(
                    title="Contribution à la CAE",
                    description=(
                        "Pourcentage que cette enseigne contribue à la CAE. Utilisé"
                        " pour les écritures et dans les calculs de coût de revient"
                        " (catalogue/étude de prix)"
                    ),
                    section="Paramètres techniques (compta, gestion)",
                ),
            ),
        ),
        group="edit",
    )
    internalcontribution = deferred(
        Column(
            Numeric(5, 2, asdecimal=False),
            info=dict(
                colanderalchemy=dict(
                    title="Contribution à la CAE pour la facturation interne",
                    description=(
                        "Pourcentage que cette enseigne contribue à la CAE"
                        " lorsqu’elle facture en interne"
                    ),
                    section="Paramètres techniques (compta, gestion)",
                ),
            ),
        ),
        group="edit",
    )
    # Coefficients de calcul pour les études de prix
    general_overhead = deferred(
        Column(
            Numeric(
                6,
                5,
                asdecimal=False,
            ),
            info=dict(
                colanderalchemy=dict(
                    title="Coefficient de frais généraux",
                    section="Coefficients de calcul des études de prix",
                    description="""
        Coefficient de frais généraux utilisé par défaut dans les études de prix pour 
        le calcul du prix de vente depuis le coût d'achat.
        Doit être compris entre 0 et 10.
        """,
                )
            ),
            default=0,
        ),
        group="edit",
    )
    margin_rate = deferred(
        Column(
            Numeric(6, 5, asdecimal=False),
            default=0,
            info=dict(
                colanderalchemy=dict(
                    title="Coefficient de marge",
                    section="Coefficients de calcul des études de prix",
                    description="""
        Coefficient de marge utilisé par défaut dans les études de prix et le catalogue 
        produit pour le calcul du prix de vente depuis le coût d'achat.
        Doit être compris entre 0 et 1.""",
                ),
            ),
        ),
        group="edit",
    )
    use_margin_rate_in_catalog = deferred(
        Column(
            Boolean(),
            default=False,
            info=dict(
                colanderalchemy=dict(
                    title="Coefficient de marge dans le catalogue ?",
                    label="Coefficient de marge dans le catalogue",
                    section="Coefficients de calcul des études de prix",
                    description="""
        Le coefficient de marge appliqué aux produits peut-il être dans le catalogue 
        (ou seulement au moment de l'étude de prix)? 
        """,
                ),
            ),
        )
    )

    # sequences related, used for counters initialization on migration from
    # another system (eg: WinScop). Contain the latest index already assigned.
    month_company_invoice_sequence_init_value = deferred(
        Column(Integer, info={"colanderalchemy": {"exclude": True}}),
    )

    month_company_invoice_sequence_init_date = deferred(
        Column(
            Date,
            info={
                "colanderalchemy": {"exclude": True},
            },
        ),
    )

    antenne_id = deferred(
        Column(
            ForeignKey("antenne_option.id", ondelete="set null"),
            info=dict(
                colanderalchemy=dict(
                    title="Antenne de rattachement",
                    section="Paramètres techniques (compta, gestion)",
                )
            ),
        )
    )

    follower_id = deferred(
        Column(
            ForeignKey("accounts.id", ondelete="set null"),
            info=dict(
                colanderalchemy=dict(
                    title="Accompagnateur de l'enseigne",
                    section="Paramètres techniques (compta, gestion)",
                    ondelete="set null",
                )
            ),
        )
    )

    # Relationships
    follower = relationship(
        "User",
        primaryjoin="User.id==Company.follower_id",
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )

    antenne = relationship(
        "AntenneOption",
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )

    header_file = relationship(
        "File",
        primaryjoin="File.id==Company.header_id",
        # backref utilisé pour le calcul des acls
        backref=backref(
            "company_header_backref",
            uselist=False,
            info={
                "colanderalchemy": {"exclude": True},
                "export": {"exclude": True},
            },
        ),
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )

    logo_file = relationship(
        "File",
        primaryjoin="File.id==Company.logo_id",
        # backref utilisé pour le calcul des acls
        backref=backref(
            "company_logo_backref",
            uselist=False,
            info={
                "colanderalchemy": {"exclude": True},
                "export": {"exclude": True},
            },
        ),
        uselist=False,
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )

    activities = relationship(
        "CompanyActivity",
        secondary=COMPANY_ACTIVITY,
        backref=backref(
            "companies",
            info={
                "colanderalchemy": {"exclude": True},
                "export": {"exclude": True},
            },
        ),
        info={
            "colanderalchemy": {
                "title": "Domaine d'activités",
                "section": "Informations publiques",
            },
            "export": {"exclude": True},
        },
    )

    customers = relationship(
        "Customer",
        primaryjoin="Company.id==Customer.company_id",
        order_by="Customer.code",
        back_populates="company",
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )

    suppliers = relationship(
        "Supplier",
        order_by="Supplier.code",
        primaryjoin="Company.id==Supplier.company_id",
        back_populates="company",
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )

    projects = relationship(
        "Project",
        primaryjoin="Project.company_id==Company.id",
        order_by="Project.id",
        back_populates="company",
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )
    tasks = relationship(
        "Task",
        primaryjoin="Task.company_id==Company.id",
        order_by="Task.date",
        back_populates="company",
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )
    employees = relationship(
        "User",
        secondary=COMPANY_EMPLOYEE,
        back_populates="companies",
        info={
            "colanderalchemy": get_excluded_colanderalchemy("Employés"),
            "export": {"exclude": True},
        },
    )
    sale_products = relationship(
        "BaseSaleProduct",
        order_by="BaseSaleProduct.label",
        back_populates="company",
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )
    expense = relationship(
        "ExpenseSheet",
        primaryjoin="ExpenseSheet.company_id==Company.id",
        order_by="ExpenseSheet.month",
        cascade="all, delete-orphan",
        back_populates="company",
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )
    supplier_invoices = relationship(
        "SupplierInvoice",
        primaryjoin="SupplierInvoice.company_id==Company.id",
        order_by="SupplierInvoice.date",
        back_populates="company",
        info={
            "colanderalchemy": {"exclude": True},
            "export": {"exclude": True},
        },
    )

    _endi_service = CompanyService

    def get_company_id(self):
        """
        Return the current company id
        Allows company id access through request's context
        """
        return self.id

    @property
    def header(self):
        return self.header_file

    @header.setter
    def header(self, appstruct):
        if self.header_file is not None and appstruct.get("delete"):
            DBSESSION().delete(self.header_file)
        else:
            if self.header_file is None:
                from endi.models.files import File

                self.header_file = File()

            appstruct["filename"] = appstruct["name"] = "header.png"
            appstruct["mimetype"] = "image/png"

            data = appstruct.pop("data", None)
            for key, value in list(appstruct.items()):
                setattr(self.header_file, key, value)

            if data is not None:
                self.header_file.data = data

            self.header_file.description = "Header"

    @property
    def logo(self):
        return self.logo_file

    @logo.setter
    def logo(self, appstruct):
        if self.logo_file is not None and appstruct.get("delete"):
            DBSESSION().delete(self.logo_file)
        else:
            if self.logo_file is None:
                from endi.models.files import File

                self.logo_file = File()

            self.logo_file.name = appstruct.get("name", "logo.png")
            for key, value in list(appstruct.items()):
                setattr(self.logo_file, key, value)
            self.logo_file.description = "Logo"

    @property
    def full_label(self):
        """
        Return the company's label to display

        Add employees infos to company's name if config ask to
        """
        return self.format_label_from_datas(self)

    @classmethod
    def query(cls, keys=None, active=True):
        """
        Return a query
        """
        if keys:
            query = DBSESSION().query(*keys)
        else:
            query = super(Company, cls).query()
        if active:
            query = query.filter(cls.active == True)  # noqa: E712
        return query.order_by(cls.name)

    def __json__(self, request):
        """
        return a dict representation
        """
        customers = [customer.__json__(request) for customer in self.customers]
        suppliers = [supplier.__json__(request) for supplier in self.suppliers]
        projects = [project.__json__(request) for project in self.projects]
        return dict(
            id=self.id,
            name=self.name,
            goal=self.goal,
            email=self.email,
            phone=self.phone,
            mobile=self.mobile,
            RIB=self.RIB,
            IBAN=self.IBAN,
            customers=customers,
            suppliers=suppliers,
            projects=projects,
            status_history=[
                status.__json__(request)
                for status in self.get_allowed_statuses(request)
            ],
        )

    def disable(self):
        """
        Disable the current company
        """
        self.active = False

    def enable(self):
        """
        enable the current company
        """
        self.active = True

    def get_tasks(self):
        """
        Get all tasks for this company, as a list
        """
        return self._endi_service.get_tasks(self)

    def get_recent_tasks(self, page_nb, nb_per_page):
        """
        :param int nb_per_page: how many to return
        :param int page_nb: pagination index

        .. todo:: this is naive, use sqlalchemy pagination

        :return: pagination for wanted tasks, total nb of tasks
        """
        count = self.get_tasks().count()
        offset = page_nb * nb_per_page
        items = self._endi_service.get_tasks(self, offset=offset, limit=nb_per_page)
        return items, count

    def get_estimations(self, valid=False):
        """
        Return the estimations of the current company
        """
        return self._endi_service.get_estimations(self, valid)

    def get_invoices(self, valid=False):
        """
        Return the invoices of the current company
        """
        return self._endi_service.get_invoices(self, valid)

    def get_cancelinvoices(self, valid=False):
        """
        Return the cancelinvoices of the current company
        """
        return self._endi_service.get_cancelinvoices(self, valid)

    def has_invoices(self):
        """
        return True if this company owns invoices
        """
        return (
            self.get_invoices(self, valid=True).count() > 0
            or self.get_cancelinvoices(self, valid=True).count() > 0
        )

    def get_real_customers(self, year):
        """
        Return the real customers (with invoices)
        """
        return self._endi_service.get_customers(self, year)

    def get_late_invoices(self):
        """
        Return invoices waiting for more than 45 days
        """
        return self._endi_service.get_late_invoices(self)

    def get_customer_codes_and_names(self):
        """
        Return current company's customer codes and names
        """
        return self._endi_service.get_customer_codes_and_names(self)

    def get_supplier_codes_and_names(self):
        """
        Return current company's supplier codes and names
        """
        return self._endi_service.get_supplier_codes_and_names(self)

    def get_project_codes_and_names(self):
        """
        Return current company's project codes and names
        """
        return self._endi_service.get_project_codes_and_names(self)

    def get_next_estimation_index(self):
        """
        Return the next estimation index
        """
        return self._endi_service.get_next_estimation_index(self)

    def get_next_invoice_index(self):
        """
        Return the next invoice index
        """
        return self._endi_service.get_next_invoice_index(self)

    def get_next_cancelinvoice_index(self):
        """
        Return the next cancelinvoice index
        """
        return self._endi_service.get_next_cancelinvoice_index(self)

    def get_turnover(self, start_date, end_date):
        """
        Retrieve the turnover for the current company on the given period
        """
        ca = self._endi_service.get_turnover(self, start_date, end_date)
        return math_utils.integer_to_amount(ca, precision=5)

    def get_total_expenses_on_period(self, start_date, end_date):
        """
        Retrieve the expense total HT for the current company on the given period
        """
        total_expenses = self._endi_service.get_total_expenses_on_period(
            self, start_date, end_date
        )
        return math_utils.integer_to_amount(total_expenses, precision=2)

    def get_nb_km_on_period(self, start_date, end_date):
        """
        Retrieve the kilometers declared for the current company on the given period
        """
        return self._endi_service.get_nb_km_on_period(self, start_date, end_date)

    def get_total_expenses_and_km_on_period(self, start_date, end_date):
        """
        Retrieve the expense total HT and the kilometers declared
        for the current company on the given period
        """
        total_expenses, nb_km = self._endi_service.get_total_expenses_and_km_on_period(
            self, start_date, end_date
        )
        return math_utils.integer_to_amount(total_expenses, precision=2), nb_km

    def get_total_purchases_on_period(self, start_date, end_date):
        """
        Retrieve the purchase total HT for the current company on the given period
        """
        total_purchases = self._endi_service.get_total_purchases_on_period(
            self, start_date, end_date
        )
        return math_utils.integer_to_amount(total_purchases, precision=2)

    def get_last_treasury_main_indicator(self):
        """
        Retrieve the main indicator's datas from the last treasury grid
        of a given company
        Return {"date", "label", "value"} of the measure
        """
        return self._endi_service.get_last_treasury_main_indicator(self)

    def set_datas_from_user(self, user_account):
        if self.antenne_id == None and user_account.userdatas != None:
            self.antenne_id = user_account.userdatas.situation_antenne_id
        if self.follower_id == None and user_account.userdatas != None:
            self.follower_id = user_account.userdatas.situation_follower_id

    """
    @classmethod
    def label_query(cls):
        return cls._endi_service.label_query(cls)

    @classmethod
    def query_for_select(cls, active_only=False):
        return cls._endi_service.query_for_select(cls, active_only)
    """

    @classmethod
    def label_datas_query(cls):
        return cls._endi_service.label_datas_query(cls)

    @classmethod
    def format_label_from_datas(cls, company_datas, with_select_search_datas=False):
        """
        company_datas:
            can be either an SqlAlchemy.Row : (
                id,
                name,
                code_compta,
                active,
                nb_employees,
                employees_list
            )
            or a dict : {
                'id': int,
                'name': str,
                'code_compta': str,
                'active': bool,
                'nb_employees': int,
                'employees_list': str
            }
        """
        return cls._endi_service.format_label_from_datas(
            cls, company_datas, with_select_search_datas
        )

    @classmethod
    def get_companies_select_datas(cls, active_only=False):
        return cls._endi_service.get_companies_select_datas(cls, active_only)

    @classmethod
    def get_id_by_analytical_account(cls, analytical_account):
        return cls._endi_service.get_id_by_analytical_account(cls, analytical_account)

    @classmethod
    def query_for_select_with_trainer(cls):
        return cls._endi_service.query_for_select_with_trainer(cls)

    def has_trainer(self):
        return self.has_group_member(group_name="trainer")

    def get_employee_ids(self):
        return self._endi_service.get_employee_ids(self)

    def get_active_employees(self):
        return self._endi_service.get_active_employees(self)

    def has_group_member(self, group_name):
        return self._endi_service.has_group_member(self, group_name)

    def employs(self, uid):
        """
        :param uiud int: User id
        """
        return self._endi_service.employs(self, uid)

    @classmethod
    def get_contribution(cls, company_id, prefix=""):
        """
        :returns: The cae contribution percentage
        """
        return cls._endi_service.get_contribution(company_id, prefix)

    @classmethod
    def get_rate(cls, company_id: int, rate_name: str, prefix: str = ""):
        """
        Récupère un taux à appliquer en fonction du nom du module
        d'écriture comptable concerné

        :param int company_id: L'id de l'enseigne
        :param str rate_name: Le nom du module d'écriture pour lequel on
        récupère le taux
        :param str prefix: préfixe de l'attribut à rechercher
        ('' ou 'internal')

        :rtype: float or None
        """
        return cls._endi_service.get_rate(company_id, rate_name, prefix)

    @classmethod
    def get_rate_level(cls, company_id: int, rate_name: str, prefix: str = ""):
        """
        Renvoie le niveau (cae/company/document) auquel la contribution est
        définie

        :param int company_id: L'id de l'enseigne
        :param str rate_name: Le nom du module d'écriture pour lequel on
        récupère le taux
        :param str prefix: préfixe de l'attribut à rechercher
        ('' ou 'internal')

        :rtype: str or None
        """
        return cls._endi_service.get_rate_level(company_id, rate_name, prefix)

    def get_general_customer_account(self, prefix=""):
        return self._endi_service.get_general_customer_account(self, prefix)

    def get_third_party_customer_account(self, prefix=""):
        return self._endi_service.get_third_party_customer_account(self, prefix)

    def get_general_supplier_account(self, prefix=""):
        return self._endi_service.get_general_supplier_account(self, prefix)

    def get_third_party_supplier_account(self, prefix=""):
        return self._endi_service.get_third_party_supplier_account(self, prefix)

    def get_general_expense_account(self, prefix=""):
        return self._endi_service.get_general_expense_account(self, prefix)

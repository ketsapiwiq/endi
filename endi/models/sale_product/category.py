import logging
from sqlalchemy import (
    Column,
    Integer,
    ForeignKey,
    Text,
    String,
)
from sqlalchemy.orm import (
    relationship,
)
from endi_base.models.base import (
    DBBASE,
    default_table_args,
)


logger = logging.getLogger(__name__)


class SaleProductCategory(DBBASE):
    """
    A product category allowing to group products
    """

    __table_args__ = default_table_args
    id = Column(Integer, primary_key=True)
    title = Column(
        String(255), nullable=False, info={"colanderalchemy": {"title": "Titre"}}
    )
    description = Column(Text(), default="")
    company_id = Column(
        ForeignKey("company.id", ondelete="CASCADE"),
        info={
            "export": {"exclude": True},
        },
    )
    company = relationship(
        "Company",
        info={
            "export": {"exclude": True},
        },
    )

    def __json__(self, request):
        """
        Json repr of our model
        """
        return dict(
            id=self.id,
            title=self.title,
            description=self.description,
            company_id=self.company_id,
        )

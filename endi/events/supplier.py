"""
Handle Supplier Document related events
"""
import logging
from zope.interface import implementer


from endi_base.mail import (
    format_link,
)
from endi.interfaces import IMailEventWrapper


logger = logging.getLogger(__name__)


ORDER_SUBJECT_TMPL = "Votre devis interne {estimation} a été validé par \
{customer}"
ORDER_BODY_TMPL = """
Bonjour {supplier},

L'enseigne {customer} a émis un bon de commande pour le devis {estimation} \
que vous avez émis.

Vous pouvez consulter votre devis ici :
{url}
"""


@implementer(IMailEventWrapper)
class InternalOrderMailStatusChangedWrapper:
    """
    Status changed mail wrapper used to signify to an internal supplier that
    his estimation was validated by the customer
    """

    def __init__(self, event):
        self.event = event
        self.request = event.request
        self.status = event.status
        self.settings = self.request.registry.settings
        self.document = event.node
        self.estimation = self.document.source_estimation

    @property
    def recipients(self):
        return self.document.supplier.email

    @property
    def sendermail(self):
        """
        Return the sender's email
        """
        if "mail.default_sender" in self.settings:
            mail = self.settings["mail.default_sender"]
        else:
            logger.info(
                "'{0}' has not set his email".format(self.event.request.user.login)
            )
            mail = "Unknown"
        return mail

    @property
    def subject(self):
        """
        return the subject of the email
        """
        return ORDER_SUBJECT_TMPL.format(
            estimation=self.estimation.name,
            customer=self.document.company.name,
        )

    @property
    def body(self):
        """
        return the body of the email
        """
        url = self.request.route_url("/estimations/{id}.html", id=self.estimation.id)
        url = format_link(self.settings, url)
        return ORDER_BODY_TMPL.format(
            supplier=self.estimation.company.name,
            customer=self.document.company.name,
            url=url,
            estimation=self.estimation.name,
        )

    def is_key_event(self):
        """
        Return True if the new status requires a mail to be sent
        """
        return self.status == "valid"

    def get_attachment(self):
        """
        Return the file data to be sent with the email
        """
        return None

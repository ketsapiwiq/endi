import os


from endi.views.admin.sale import SaleIndexView, SALE_URL
from endi.views.admin.tools import BaseAdminIndexView


PDF_URL = os.path.join(SALE_URL, "pdf")


class PdfIndexView(BaseAdminIndexView):
    title = "Sorties PDF"
    description = "Configurer les mentions générales des sorties pdf"
    route_name = PDF_URL


def includeme(config):
    config.add_route(PDF_URL, PDF_URL)
    config.add_admin_view(
        PdfIndexView,
        parent=SaleIndexView,
    )
    config.include(".common")
    config.include(".estimation")
    config.include(".invoice")

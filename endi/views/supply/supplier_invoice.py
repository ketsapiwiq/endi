import logging
from typing import Dict

from pyramid.httpexceptions import HTTPFound
import deform_extensions

from endi import forms
from endi.forms.supply.supplier_invoice import (
    get_supplier_invoice_add_by_supplier_schema,
    SupplierInvoiceAddByOrdersSchema,
    SupplierPaymentSchema,
    SupplierInvoiceDispatchSchema,
    UserPaymentSchema,
    SetTypesSchema,
)
from endi.forms.supply.supplier_invoice import get_supplier_invoice_list_schema
from endi.forms.company import get_default_employee_from_request
from endi.events.files import (
    FileAdded,
)
from endi.models.company import Company
from endi.models.files import File
from endi.models.supply import (
    SupplierInvoice,
    SupplierInvoiceLine,
    SupplierOrder,
    SupplierInvoiceSupplierPayment,
)
from endi.models.supply.payment import SupplierInvoiceUserPayment
from endi.models.third_party.supplier import Supplier
from endi.resources import (
    dispatch_supplier_invoice_js,
    supplier_invoice_resources,
)
from endi.utils.widgets import (
    Link,
    POSTButton,
    ViewLink,
)
from endi.views import (
    BaseAddView,
    BaseListView,
    BaseFormView,
    BaseView,
    DeleteView,
    submit_btn,
    JsAppViewMixin,
)
from endi.views.files.views import FileUploadView
from endi.views.supply import SupplierDocListTools
from sqlalchemy import desc, asc
from sqlalchemy.orm import contains_eager

logger = logging.getLogger(__name__)


def populate_actionmenu(request):
    return request.actionmenu.add(
        ViewLink(
            "Revenir à la liste des factures fournisseur",
            path="/company/{id}/suppliers_invoices",
            id=request.context.get_company_id(),
        )
    )


class BaseSupplierInvoiceAddMixin(BaseFormView):
    add_template_vars = ("title",)
    title = "Saisir une facture fournisseur"

    buttons = (submit_btn,)


class SupplierInvoiceAddView(BaseSupplierInvoiceAddMixin, BaseFormView):
    """
    Can lead (redirect) to SupplierInvoice Creation or to supplier selection.
    """

    schema = SupplierInvoiceAddByOrdersSchema()

    def submit_success(self, appstruct):
        company = self.context

        suppliers_orders_ids = list(appstruct.pop("suppliers_orders_ids", set()))
        if len(suppliers_orders_ids) > 0:
            first_order = SupplierOrder.get(suppliers_orders_ids[0])
            supplier = Supplier.get(first_order.supplier_id)
            obj = SupplierInvoice(
                supplier_id=supplier.id,
                company_id=company.id,
                payer=get_default_employee_from_request(self.request),
            )
            self.dbsession.add(obj)

            for order_id in suppliers_orders_ids:
                order = SupplierOrder.get(order_id)
                order.supplier_invoice = obj
                self.dbsession.merge(order)
                # validator already ensured that cae_percentage are the same
                # among linked orders.
                obj.cae_percentage = order.cae_percentage

                obj.import_lines_from_order(order)

            self.dbsession.merge(obj)

            self.dbsession.flush()

            msg = "La facture a été créée, les lignes ont été copiées depuis "
            if len(suppliers_orders_ids) < 2:
                msg += "la commande fournisseur."
            else:
                msg += "les commandes fournisseurs."
            self.request.session.flash(msg)

            redirect_url = self.request.route_path(
                "/suppliers_invoices/{id}",
                id=obj.id,
            )
        else:
            redirect_url = (
                self.request.route_path(
                    "/company/{id}/suppliers_invoices", id=company.id
                )
                + "?action=new_step2"
            )
        return HTTPFound(redirect_url)


class SupplierInvoiceAddStep2View(BaseSupplierInvoiceAddMixin, BaseAddView):
    """
    Optional view ; with supplier selector

    Displayed only when no supplier order has been selected at previous step.
    """

    msg = "La facture fournisseur a été créée"

    schema = get_supplier_invoice_add_by_supplier_schema()

    def create_instance(self):
        return SupplierInvoice(
            company_id=self.context.id,
            payer=get_default_employee_from_request(self.request),
        )

    def redirect(self, obj):
        url = self.request.route_path(
            "/suppliers_invoices/{id}",
            id=obj.id,
        )
        return HTTPFound(url)


class SupplierInvoiceEditView(BaseView, JsAppViewMixin):
    """
    Can act as edit view or readonly view (eg: waiting for validation).
    """

    def context_url(self, _query: Dict[str, str] = {}):
        return self.request.route_path(
            "/api/v1/suppliers_invoices/{id}", id=self.request.context.id, _query=_query
        )

    @property
    def title(self):
        label = self.context.remote_invoice_number
        if self.context.internal:
            label += " (Facture interne)"
        return label

    def more_js_app_options(self):
        return dict(
            edit=bool(self.request.has_permission("edit.supplier_invoice")),
        )

    def __call__(self):
        populate_actionmenu(self.request)
        supplier_invoice_resources.need()
        return dict(
            title=self.title,
            context=self.context,
            js_app_options=self.get_js_app_options(),
        )


class SupplierInvoiceListTools(SupplierDocListTools):
    model_class = SupplierInvoice
    sort_columns = {
        "official_number": "official_number",
        "remote_invoice_number": "remote_invoice_number",
        "total_ht": "total_ht",
        "total_tva": "total_tva",
        "total": "total",
    }
    sort_columns.update(SupplierDocListTools.sort_columns)

    # sort by invoice date rather than creation date
    default_sort = "date"
    default_direction = "desc"

    def sort_by_total_tva(self, query, appstruct):
        sort_direction = self._get_sort_direction(appstruct)
        self.logger.debug("  + Direction : %s" % sort_direction)
        query = query.outerjoin(SupplierInvoice.lines).options(
            contains_eager(SupplierInvoice.lines).load_only(
                SupplierInvoiceLine.tva,
            )
        )

        if sort_direction == "asc":
            func = asc
        else:
            func = desc

        query = query.order_by(func(SupplierInvoiceLine.tva))
        return query

    def sort_by_total_ht(self, query, appstruct):
        sort_direction = self._get_sort_direction(appstruct)
        self.logger.debug("  + Direction : %s" % sort_direction)
        query = query.outerjoin(SupplierInvoice.lines).options(
            contains_eager(SupplierInvoice.lines).load_only(
                SupplierInvoiceLine.ht,
            )
        )

        if sort_direction == "asc":
            func = asc
        else:
            func = desc

        query = query.order_by(func(SupplierInvoiceLine.ht))
        return query

    def sort_by_total(self, query, appstruct):
        sort_direction = self._get_sort_direction(appstruct)
        self.logger.debug("  + Direction : %s" % sort_direction)
        query = query.outerjoin(SupplierInvoice.lines).options(
            contains_eager(SupplierInvoice.lines).load_only(
                SupplierInvoiceLine.ht, SupplierInvoiceLine.tva
            )
        )

        if sort_direction == "asc":
            func = asc
        else:
            func = desc

        query = query.order_by(func(SupplierInvoiceLine.ht + SupplierInvoiceLine.tva))
        return query

    def filter_official_number(self, query, appstruct):
        official_number = appstruct.get("official_number")
        if official_number:
            query = query.filter_by(official_number=official_number)
        return query

    def filter_remote_invoice_number(self, query, appstruct):
        remote_invoice_number = appstruct.get("remote_invoice_number")
        if remote_invoice_number:
            query = query.filter_by(remote_invoice_number=remote_invoice_number)
        return query

    def filter_combined_paid_status(sef, query, appstruct):
        status = appstruct.get("combined_paid_status")
        if status == "paid":
            query = query.filter_by(paid_status="resulted")
        elif status == "supplier_topay":
            query = query.filter(
                SupplierInvoice.supplier_paid_status != "resulted",
                SupplierInvoice.cae_percentage > 0,
            )
        elif status == "worker_topay":
            query = query.filter(
                SupplierInvoice.worker_paid_status != "resulted",
                SupplierInvoice.cae_percentage < 100,
            )
        return query


def stream_supplier_invoice_actions(request, supplier_invoice):
    yield Link(
        request.route_path(
            "/suppliers_invoices/{id}",
            id=supplier_invoice.id,
        ),
        "Voir ou modifier",
        icon="arrow-right",
    )
    delete_allowed = request.has_permission(
        "delete.supplier_invoice",
        supplier_invoice,
    )
    if delete_allowed:
        yield POSTButton(
            request.route_path(
                "/suppliers_invoices/{id}",
                id=supplier_invoice.id,
                _query=dict(action="delete"),
            ),
            "Supprimer",
            title="Supprimer définitivement cette facture ?",
            icon="trash-alt",
            css="negative",
            confirm="Êtes-vous sûr de vouloir supprimer cette facture ?",
        )


class BaseSupplierInvoiceListView(
    SupplierInvoiceListTools,
    BaseListView,
):
    title = "Liste des factures fournisseurs"
    add_template_vars = ["title", "stream_actions"]

    def stream_actions(self, supplier_invoice):
        return stream_supplier_invoice_actions(self.request, supplier_invoice)


class CompanySupplierInvoiceListView(BaseSupplierInvoiceListView):
    """
    Company-scoped list of SupplierOrder
    """

    schema = get_supplier_invoice_list_schema(is_global=False)

    def query(self):
        company = self.request.context
        query = SupplierInvoice.query()
        return query.filter_by(company_id=company.id)


class AdminSupplierInvoiceListView(BaseSupplierInvoiceListView):
    """
    Global list of SupplierOrder from all companies
    """

    is_admin_view = True
    add_template_vars = BaseSupplierInvoiceListView.add_template_vars + [
        "is_admin_view",
    ]

    schema = get_supplier_invoice_list_schema(is_global=True)

    def query(self):
        return SupplierInvoice.query()


class SupplierInvoiceDeleteView(DeleteView):
    delete_msg = "La facture fournisseur a bien été supprimée"

    def redirect(self):
        return HTTPFound(
            self.request.route_path(
                "/company/{id}/suppliers_invoices", id=self.context.company.id
            )
        )


class BaseSupplierInvoicePaymentView(BaseFormView):
    # Must be defined by subclasses
    PaymentClass = None
    success_msg = ""

    def before(self, form):
        populate_actionmenu(self.request)

    def redirect(self, come_from):
        if come_from:
            return HTTPFound(come_from)
        else:
            return HTTPFound(
                self.request.route_path(
                    "/suppliers_invoices/{id}", id=self.request.context.id
                )
            )

    def submit_success(self, appstruct):
        """
        Create the payment
        """
        logger.debug(f"+ Submitting a {self.PaymentClass.__name__} payment")
        logger.debug(appstruct)
        come_from = appstruct.pop("come_from", None)
        force_resulted = appstruct.pop("resulted", None)
        supplier_payment = self.PaymentClass(**appstruct)
        supplier_payment.user_id = self.request.user.id
        self.context.record_payment(
            supplier_payment,
            force_resulted=force_resulted,
        )

        self.dbsession.merge(self.context)
        self.request.session.flash(self.success_msg)
        # FIXME
        # notify_status_changed(self.request, self.context.paid_status)
        return self.redirect(come_from)


class SupplierPaymentView(BaseSupplierInvoicePaymentView):
    """
    Called for setting a payment to an user on a SupplierInvoice
    """

    schema = SupplierPaymentSchema()
    title = "Saisie d'un paiement fournisseur"
    success_msg = "Le paiement a bien été enregistré"
    PaymentClass = SupplierInvoiceSupplierPayment


class UserPaymentView(BaseSupplierInvoicePaymentView):
    """
    Called for setting a payment to a supplier on a SupplierInvoice
    """

    schema = UserPaymentSchema()
    title = "Saisie d'un remboursement entrepreneur"
    success_msg = "Le remboursement a bien été enregistré"
    PaymentClass = SupplierInvoiceUserPayment


SUPPLIER_INVOICE_DISPATCH_GRID = (
    (
        ("date", 2),
        ("supplier_id", 10),
    ),
    (("remote_invoice_number", 6), ("invoice_file", 6)),
    (
        ("total_ht", 6),
        ("total_tva", 6),
    ),
    (("lines", 12),),
)


class SupplierInvoiceDispatchView(BaseFormView):
    """
    Used when an EA receives a global supplier invoice that needs to be split,
    enDi-wise, into several supplier invoices.
    """

    add_template_vars = ("title",)
    title = "Ventiler une facture fournisseur"
    schema = SupplierInvoiceDispatchSchema(
        widget=deform_extensions.GridFormWidget(
            named_grid=SUPPLIER_INVOICE_DISPATCH_GRID
        ),
        title="Réception d'une commande fournisseur",
    )

    def before(self, form):
        dispatch_supplier_invoice_js.need()

    @staticmethod
    def _group_lines_by_company(lines):
        ret = {}
        for line in lines:
            try:
                ret[line["company_id"]].append(line)
            except KeyError:
                ret[line["company_id"]] = [line]
        return ret

    def submit_success(self, appstruct):
        reference_supplier = Supplier.query().get(appstruct["supplier_id"])
        created_invoices = []

        indexed_lines = self._group_lines_by_company(appstruct["lines"])
        for company_id, lines in list(indexed_lines.items()):
            supplier = (
                Supplier.query()
                .filter_by(
                    registration=reference_supplier.registration,
                    company_id=company_id,
                )
                .first()
            )
            if supplier is None:
                # Copy minimal information to avoid data leak
                supplier = Supplier(
                    company_id=company_id,
                    company_name=reference_supplier.company_name,
                    registration=reference_supplier.registration,
                )
                self.dbsession.add(supplier)

            invoice = SupplierInvoice(
                date=appstruct["date"],
                company=Company.get(company_id),
                supplier=supplier,
                remote_invoice_number=appstruct.get("remote_invoice_number", ""),
            )

            f = File(parent=invoice, name=appstruct["invoice_file"]["name"])
            forms.merge_session_with_post(f, appstruct["invoice_file"])
            self.dbsession.add(f)
            self.request.registry.notify(FileAdded(self.request, f))

            self.dbsession.add(invoice)

            for line in lines:
                SupplierInvoiceLine(
                    supplier_invoice=invoice,
                    description=line["description"],
                    ht=line["ht"],
                    tva=line["tva"],
                    type_id=line["type_id"],
                )
                # invoice.lines.append(new_line)
            created_invoices.append(invoice)

        invoices_descriptions = [
            f"{invoice.company.name}/{invoice.remote_invoice_number}"
            for invoice in created_invoices
        ]

        self.session.flash(
            "Les factures suivantes ont été créées : {}".format(
                " ".join(invoices_descriptions)
            )
        )
        return HTTPFound("/suppliers_invoices")


class SupplierInvoiceSetTypesView(BaseFormView):
    """
    Base view for setting product codes (on supplier_invoices)

    context

        invoice or cancelinvoice
    """

    schema = SetTypesSchema()

    @property
    def title(self):
        return (
            "Configuration des types de dépenses de la facture fournisseur "
            "{}".format(self.context.official_number)
        )

    def before(self, form):
        form.set_appstruct({"lines": [line.appstruct() for line in self.context.lines]})
        self.request.actionmenu.add(
            ViewLink(
                "Revenir au document",
                path="/suppliers_invoices/{id}",
                id=self.context.id,
            )
        )

    def submit_success(self, appstruct):
        for line in appstruct["lines"]:
            line_id = line.get("id")
            type_id = line.get("type_id")
            if line_id is not None and type_id is not None:
                line = SupplierInvoiceLine.get(line_id)
                if line.supplier_invoice == self.context:
                    line.type_id = type_id
                    self.request.dbsession.merge(line)
                else:
                    logger.error(
                        "Possible break in attempt: trying to set product id "
                        "on the wrong supplier_invoice line (not belonging to "
                        "this supplier_invoice)"
                    )
        return HTTPFound(
            self.request.route_path("/suppliers_invoices/{id}", id=self.context.id)
        )


def add_routes(config):
    config.add_route(
        "/suppliers_invoices",
        "/suppliers_invoices",
    )

    config.add_route(
        "/company/{id}/suppliers_invoices",
        "/company/{id}/suppliers_invoices",
        traverse="/companies/{id}",
    )

    config.add_route(
        "/suppliers_invoices/{id}",
        "/suppliers_invoices/{id}",
        traverse="/suppliers_invoices/{id}",
    )
    for action in (
        "delete",
        "add_supplier_payment",
        "add_user_payment",
        "addfile",
        "set_types",
    ):
        config.add_route(
            "/suppliers_invoices/{id}/%s" % action,
            r"/suppliers_invoices/{id:\d+}/%s" % action,
            traverse="/suppliers_invoices/{id}",
        )
    config.add_route(
        "/dispatch_supplier_invoice",
        "/dispatch_supplier_invoice",
    )


def add_views(config):
    # Admin views
    config.add_view(
        AdminSupplierInvoiceListView,
        request_method="GET",
        route_name="/suppliers_invoices",
        permission="admin.supplier_invoice",
        renderer="/supply/suppliers_invoices.mako",
    )

    config.add_view(
        SupplierInvoiceAddView,
        route_name="/company/{id}/suppliers_invoices",
        request_param="action=new",
        permission="add.supplier_invoice",
        renderer="base/formpage.mako",
    )
    config.add_view(
        SupplierInvoiceAddStep2View,
        route_name="/company/{id}/suppliers_invoices",
        request_param="action=new_step2",
        permission="add.supplier_invoice",
        renderer="base/formpage.mako",
    )
    config.add_view(
        CompanySupplierInvoiceListView,
        route_name="/company/{id}/suppliers_invoices",
        request_method="GET",
        renderer="/supply/suppliers_invoices.mako",
        permission="list.supplier_invoice",
    )
    config.add_view(
        SupplierInvoiceEditView,
        route_name="/suppliers_invoices/{id}",
        renderer="/supply/supplier_invoice.mako",
        permission="view.supplier_invoice",
        layout="opa",
    )
    config.add_view(
        SupplierInvoiceSetTypesView,
        route_name="/suppliers_invoices/{id}/set_types",
        renderer="base/formpage.mako",
        permission="set_types.supplier_invoice",
    )

    config.add_view(
        SupplierInvoiceDeleteView,
        route_name="/suppliers_invoices/{id}",
        request_param="action=delete",
        permission="delete.supplier_invoice",
        request_method="POST",
        require_csrf=True,
    )

    config.add_view(
        SupplierPaymentView,
        route_name="/suppliers_invoices/{id}/add_supplier_payment",
        permission="add_payment.supplier_invoice",
        renderer="base/formpage.mako",
    )

    config.add_view(
        UserPaymentView,
        route_name="/suppliers_invoices/{id}/add_user_payment",
        permission="add_payment.supplier_invoice",
        renderer="base/formpage.mako",
    )

    # File attachment
    config.add_view(
        FileUploadView,
        route_name="/suppliers_invoices/{id}/addfile",
        renderer="base/formpage.mako",
        permission="add.file",
    )

    config.add_view(
        SupplierInvoiceDispatchView,
        route_name="/dispatch_supplier_invoice",
        permission="admin.supplier_invoice",
        renderer="supply/dispatch_supplier_invoice.mako",
    )


def includeme(config):
    add_routes(config)
    add_views(config)
    config.add_admin_menu(
        parent="sale",
        order=3,
        label="Factures fournisseurs",
        href="/suppliers_invoices",
        routes_prefixes=["/suppliers_invoices/{id}"],
    )
    config.add_company_menu(
        parent="supply",
        order=2,
        label="Factures fournisseurs",
        route_name="/company/{id}/suppliers_invoices",
        route_id_key="company_id",
        routes_prefixes=["/suppliers_invoices/{id}"],
    )

from colanderalchemy import SQLAlchemySchemaNode

from endi.models.status import StatusLogEntry
from endi.views import BaseRestView


class StatusLogEntryRestView(BaseRestView):
    """
    For StatusLogEntry with a parent model inheriting Node.

    We expect context to hold a `statuses` attr with all status log entries.
    """

    def post_format(self, entry: StatusLogEntry, edit: bool, attributes):
        if not edit:
            entry.node = self.context
            entry.user = self.request.user
        return entry

    def collection_get(self):
        return self.context.statuses

    def get_schema(self, submitted):
        return SQLAlchemySchemaNode(
            StatusLogEntry, includes=("label", "comment", "visibility", "pinned")
        )

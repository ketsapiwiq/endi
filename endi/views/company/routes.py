from endi.views import redirect_to_index_view

COLLECTION_ROUTE = "/companies"
ITEM_ROUTE = "/companies/{id}"
DASHBOARD_ROUTE = "/companies/{id}/dashboard"
OLD_DASHBOARD_ROUTE = "/company/{id}/dashboard"


def includeme(config):
    """
    Configure routes for this module
    """
    config.add_route(COLLECTION_ROUTE, COLLECTION_ROUTE)
    traverse = "/companies/{id}"
    config.add_route(ITEM_ROUTE, ITEM_ROUTE, traverse=traverse)
    config.add_route(DASHBOARD_ROUTE, DASHBOARD_ROUTE, traverse=traverse)
    # Ancienne vue
    config.add_route(OLD_DASHBOARD_ROUTE, OLD_DASHBOARD_ROUTE, traverse=traverse)
    config.add_view(redirect_to_index_view, route_name=OLD_DASHBOARD_ROUTE)

    config.add_route(
        "/api/v1/companies/{id}",
        "/api/v1/companies/{id}",
        traverse="/companies/{id}",
    )
    config.add_route(
        "/api/v1/companies/{id}/statuslogentries",
        r"/api/v1/companies/{id:\d+}/statuslogentries",
        traverse="/companies/{id}",
    )
    config.add_route(
        "/api/v1/companies/{eid}/statuslogentries/{id}",
        r"/api/v1/companies/{eid:\d+}/statuslogentries/{id:\d+}",
        traverse="/statuslogentries/{id}",
    )

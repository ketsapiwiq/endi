import os

COMPANY_CUSTOMERS = "/company/{id}/customers"
COMPANY_CUSTOMERS_ADD = os.path.join(COMPANY_CUSTOMERS, "add")


def includeme(config):
    config.add_route(
        "customer",
        "/customers/{id}",
        traverse="/customers/{id}",
    )
    config.add_route(
        "/api/v1/companies/{id}/customers",
        "/api/v1/companies/{id}/customers",
        traverse="/companies/{id}",
    )
    config.add_route(
        "/api/v1/customers/{id}",
        "/api/v1/customers/{id}",
        traverse="/customers/{id}",
    )

    config.add_route(
        "/api/v1/customers/{id}/statuslogentries",
        r"/api/v1/customers/{id:\d+}/statuslogentries",
        traverse="/customers/{id}",
    )

    config.add_route(
        "/api/v1/customers/{eid}/statuslogentries/{id}",
        r"/api/v1/customers/{eid:\d+}/statuslogentries/{id:\d+}",
        traverse="/statuslogentries/{id}",
    )

    for route in (COMPANY_CUSTOMERS, COMPANY_CUSTOMERS_ADD):
        config.add_route(route, route, traverse="/companies/{id}")

    config.add_route(
        "customers.csv", r"/company/{id:\d+}/customers.csv", traverse="/companies/{id}"
    )

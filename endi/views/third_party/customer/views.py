import itertools
import logging
import re
import typing
import colander
from pyramid.decorator import reify
from pyramid.httpexceptions import HTTPFound
import deform

from deform_extensions import GridFormWidget

from endi.models.third_party.customer import Customer
from endi.models.project.project import Project
from endi.models.config import Config
from endi.resources import node_view_only_js

from endi.views import (
    BaseEditView,
    BaseFormView,
    BaseView,
    submit_btn,
    cancel_btn,
    JsAppViewMixin,
)
from endi.utils.widgets import (
    Link,
    POSTButton,
)
from endi.forms.third_party.customer import (
    CustomerAddToProjectSchema,
    get_company_customer_schema,
    get_individual_customer_schema,
    get_internal_customer_schema,
)
from endi.views.project.routes import (
    COMPANY_PROJECTS_ROUTE,
)
from endi.views.csv_import import (
    CsvFileUploadView,
    ConfigFieldAssociationView,
)
from .base import (
    populate_actionmenu,
    FORM_GRIDS,
)
from .routes import COMPANY_CUSTOMERS, COMPANY_CUSTOMERS_ADD

logger = logging.getLogger(__name__)


class CustomerView(BaseFormView, JsAppViewMixin):
    """
    Return the view of a customer
    """

    def get_company_projects_form(self):
        """
        Return a form object for project add
        :param obj request: The pyramid request object
        :returns: A form
        :rtype: class:`deform.Form`
        """
        schema = CustomerAddToProjectSchema().bind(
            request=self.request, context=self.context
        )
        form = deform.Form(
            schema,
            buttons=(submit_btn,),
            action=self.request.route_path(
                "customer", id=self.context.id, _query={"action": "addcustomer"}
            ),
        )
        return form

    def _get_buttons(self):
        yield Link(
            self.request.route_path(
                "customer",
                id=self.context.id,
                _query={"action": "edit"},
            ),
            "Modifier",
            title="",
            icon="pen",
            css="btn btn-primary",
        )
        if self.request.has_permission("delete_customer"):
            yield POSTButton(
                self.request.route_path(
                    "customer",
                    id=self.context.id,
                    _query=dict(action="delete"),
                ),
                "Supprimer",
                title="Supprimer définitivement ce client",
                icon="trash-alt",
                css="negative",
                confirm="Êtes-vous sûr de vouloir supprimer ce client ?",
            )
        elif self.request.has_permission("edit_customer"):
            if self.context.archived:
                label = "Désarchiver"
                css = ""
            else:
                label = "Archiver"
                css = "negative"

            yield POSTButton(
                self.request.route_path(
                    "customer",
                    id=self.context.id,
                    _query=dict(action="archive"),
                ),
                label,
                icon="archive",
                css=css,
            )

    def context_url(self, _query: typing.Dict[str, str] = {}):
        return self.request.route_url(
            "/api/v1/customers/{id}", id=self.context.id, _query=_query
        )

    def __call__(self):
        populate_actionmenu(self.request)
        node_view_only_js.need()

        title = "Client : {0}".format(self.context.label)
        if self.request.context.code:
            title += " {0}".format(self.context.code)

        return dict(
            title=title,
            customer=self.request.context,
            project_form=self.get_company_projects_form(),
            add_project_url=self.request.route_path(
                COMPANY_PROJECTS_ROUTE,
                id=self.context.company.id,
                _query={"action": "add", "customer": self.context.id},
            ),
            actions=list(self._get_buttons()),
            js_app_options=self.get_js_app_options(),
        )


def customer_archive(request):
    """
    Archive the current customer
    """
    customer = request.context
    if not customer.archived:
        customer.archived = True
    else:
        customer.archived = False
    request.dbsession.merge(customer)
    return HTTPFound(request.referer)


def customer_delete(request):
    """
    Delete the current customer
    """
    customer = request.context
    company_id = customer.company_id
    request.dbsession.delete(customer)
    request.session.flash("Le client '{0}' a bien été supprimé".format(customer.label))
    # On s'assure qu'on ne redirige pas vers la route courante
    if re.compile(".*customers/[0-9]+.*").match(request.referer):
        redirect = request.route_path(COMPANY_CUSTOMERS, id=company_id)
    else:
        redirect = request.referer
    return HTTPFound(redirect)


class CustomerAddToProject(BaseFormView):
    """
    Catch customer id and update project customers
    """

    schema = CustomerAddToProjectSchema()
    validation_msg = "Le dossier a été ajouté avec succès"

    def submit_success(self, appstruct):
        project_id = appstruct["project_id"]
        project = self.dbsession.query(Project).filter_by(id=project_id).one()
        customer = self.context
        project.customers.append(customer)
        self.dbsession.flush()
        self.session.flash(self.validation_msg)
        redirect = self.request.route_path(
            "customer",
            id=self.context.id,
        )
        return HTTPFound(redirect)


class CustomerAddEditController:
    def __init__(self, request, edit=False):
        self.request = request
        self.context = request.context
        self.edit = edit
        self._schema = None

    def get_company_schema(self):
        return get_company_customer_schema()

    def get_individual_schema(self):
        return get_individual_customer_schema()

    def get_internal_schema(self):
        return get_internal_customer_schema(edit=self.edit)

    def get_customer_type(self) -> str:
        if self.edit:
            customer_type = self.context.type
        else:
            customer_type = self.request.params.get("__formid__", "company")
        return customer_type

    def get_schema(self) -> colander.Schema:
        """
        Build and cache the current schema
        """
        if self._schema is None:
            customer_type = self.get_customer_type()
            method = f"get_{customer_type}_schema"
            self._schema = getattr(self, method)()
        return self._schema

    def add_customer(self, schema: colander.Schema, appstruct: dict) -> Customer:
        customer = schema.objectify(appstruct)
        customer.company = self.context
        customer.type = self.get_customer_type()
        self.request.dbsession.add(customer)
        self.request.dbsession.flush()
        return customer

    def edit_customer(self, schema: colander.Schema, appstruct: dict) -> Customer:
        customer = schema.objectify(appstruct, self.context)
        customer = self.request.dbsession.merge(customer)
        self.request.dbsession.flush()
        return customer


class CustomerAddView(BaseView):
    """
    Customer Add view (multiple forms)
    """

    title = "Ajouter un client"
    buttons = (
        submit_btn,
        cancel_btn,
    )
    controller_class = CustomerAddEditController

    def __init__(self, context, request=None):
        super().__init__(context, request)
        self.controller = self.controller_class(self.request, edit=False)
        self.counter = itertools.count()
        # Dans le cas d'un échec lors d'un submit, on veut pouvoir renvoyer
        # l'instance de formulaire en erreur
        self._form_cache = {}

    def _get_company_customer_form(self) -> deform.Form:
        formid = "company"
        if formid not in self._form_cache:
            schema = self.controller.get_company_schema()
            schema = schema.bind(request=self.request)
            form = deform.Form(
                schema=schema,
                formid="company",
                buttons=self.buttons,
                counter=self.counter,
            )
            form.title = "Personne morale"
            form.widget = GridFormWidget(named_grid=FORM_GRIDS["company"])
            self._form_cache[formid] = form
        return self._form_cache[formid]

    def _get_individual_customer_form(self) -> deform.Form:
        formid = "individual"
        if formid not in self._form_cache:
            schema = self.controller.get_individual_schema()
            schema = schema.bind(request=self.request)
            form = deform.Form(
                schema=schema,
                formid="individual",
                buttons=self.buttons,
                counter=self.counter,
            )
            form.title = "Personne physique"
            form.widget = GridFormWidget(named_grid=FORM_GRIDS["individual"])
            self._form_cache[formid] = form
        return self._form_cache[formid]

    def _get_internal_customer_form(self) -> typing.Optional[deform.Form]:
        if Config.get_value("internal_invoicing_active", default=True, type_=bool):
            formid = "internal"
            if formid not in self._form_cache:
                schema = self.controller.get_internal_schema()
                schema = schema.bind(request=self.request)
                form = deform.Form(
                    schema=schema,
                    formid="internal",
                    buttons=self.buttons,
                    counter=self.counter,
                )
                form.title = "Enseigne de la CAE"
                form.widget = GridFormWidget(named_grid=FORM_GRIDS["internal"])
                self._form_cache[formid] = form
        return self._form_cache[formid]

    def _get_forms(self):
        result = [
            (
                "company",
                self._get_company_customer_form(),
            ),
            ("individual", self._get_individual_customer_form()),
        ]

        internal_form = self._get_internal_customer_form()
        if internal_form:
            result.append(("internal", internal_form))
        return result

    def get_current_form_name(self) -> str:
        return self.request.params.get("__formid__", "company")

    def get_current_form(self) -> deform.Form:
        form_name = self.controller.get_customer_type()
        method = f"_get_{form_name}_customer_form"
        return getattr(self, method)()

    @property
    def customers(self):
        codes = self.context.get_customer_codes_and_names()
        return codes

    def handle_success(self, form, appstruct: dict):
        customer = self.controller.add_customer(form.schema, appstruct)
        self.session.flash(f"Le client {customer.label} a bien été ajouté")
        return HTTPFound(self.request.route_path("customer", id=customer.id))

    def handle_fail(self, form, appstruct: dict):
        self.session.flash("Il y a une erreur dans votre saisie", queue="error")
        return self.get_template_context()

    def _handle_submit(self) -> typing.Union[dict, HTTPFound]:
        """
        Handle the submission of one of the forms
        Return Template context data
        """
        form = self.get_current_form()
        try:
            controls = self.request.POST.items()
            validated = form.validate(controls)
            result = self.handle_success(form, validated)
        except deform.exception.ValidationFailure as e:
            logger.exception("Erreur à la valdiation du formulaire :")
            logger.error(e.error)
            result = self.handle_fail(form, e)
        return result

    def get_template_context(self) -> dict:
        return dict(
            title=self.title,
            forms=self._get_forms(),
            customers=self.customers,
            current_form=self.get_current_form_name(),
        )

    def __call__(self):
        if "submit" in self.request.params:
            return self._handle_submit()
        elif "cancel" in self.request.params:
            return HTTPFound(
                self.request.route_path(COMPANY_CUSTOMERS, id=self.context.id)
            )
        else:
            return self.get_template_context()


class CustomerEditView(BaseEditView):
    """
    Customer edition form
    """

    add_template_vars = (
        "title",
        "customers",
    )
    buttons = (
        submit_btn,
        cancel_btn,
    )
    _schema = None
    validation_msg = "Le client a été modifié avec succès"
    redirect_route = "customer"
    controller_class = CustomerAddEditController

    def __init__(self, context, request=None):
        super().__init__(context, request)
        self.controller = self.controller_class(self.request, edit=True)

    def get_schema(self) -> colander.Schema:
        """
        Build and cache the current schema
        """
        return self.controller.get_schema()

    @reify
    def title(self):
        return "Modifier le client '{0}' de l'enseigne '{1}'".format(
            self.context.name, self.context.company.name
        )

    @property
    def customers(self):
        company = self.context.company
        codes = company.get_customer_codes_and_names()
        codes = codes.filter(Customer.id != self.context.id)
        return codes

    @property
    def named_form_grid(self):
        """
        Collect the form's grid (functionnalité provided by the parent class)
        """
        return FORM_GRIDS[self.context.type]

    def submit_success(self, appstruct):
        model = self.controller.edit_customer(self.schema, appstruct)
        self.session.flash(self.validation_msg)
        return HTTPFound(self.request.route_path("customer", id=model.id))

    def cancel_success(self, appstruct):
        return HTTPFound(self.request.route_path("customer", id=self.context.id))


class CustomerImportStep1(CsvFileUploadView):
    title = "Import des clients, étape 1 : chargement d'un fichier au \
format csv"
    model_types = ("customers",)
    default_model_type = "customers"

    def get_next_step_route(self, args):
        return self.request.route_path(
            "company_customers_import_step2", id=self.context.id, _query=args
        )


class CustomerImportStep2(ConfigFieldAssociationView):

    title = "Import de clients, étape 2 : associer les champs"
    model_types = CustomerImportStep1.model_types

    def get_previous_step_route(self):
        return self.request.route_path(
            "company_customers_import_step1",
            id=self.context.id,
        )

    def get_default_values(self):
        logger.info("Asking for default values : %s" % self.context.id)
        return dict(company_id=self.context.id)


def includeme(config):
    """
    Add module's views
    """
    for i in range(2):
        index = i + 1
        route_name = "company_customers_import_step%d" % index
        path = r"/company/{id:\d+}/customers/import/%d" % index
        config.add_route(route_name, path, traverse="/companies/{id}")

    config.add_view(
        CustomerAddView,
        route_name=COMPANY_CUSTOMERS_ADD,
        renderer="customers/add.mako",
        permission="add_customer",
    )
    config.add_view(
        CustomerEditView,
        route_name="customer",
        renderer="customers/edit.mako",
        request_param="action=edit",
        permission="edit_customer",
    )

    config.add_view(
        CustomerView,
        route_name="customer",
        renderer="customers/view.mako",
        request_method="GET",
        permission="view_customer",
    )
    config.add_view(
        customer_delete,
        route_name="customer",
        request_param="action=delete",
        permission="delete_customer",
        request_method="POST",
        require_csrf=True,
    )
    config.add_view(
        customer_archive,
        route_name="customer",
        request_param="action=archive",
        permission="edit_customer",
        request_method="POST",
        require_csrf=True,
    )

    config.add_view(
        CustomerImportStep1,
        route_name="company_customers_import_step1",
        permission="add_customer",
        renderer="base/formpage.mako",
    )

    config.add_view(
        CustomerImportStep2,
        route_name="company_customers_import_step2",
        permission="add_customer",
        renderer="base/formpage.mako",
    )
    config.add_view(
        CustomerAddToProject,
        route_name="customer",
        request_param="action=addcustomer",
        permission="edit_customer",
        renderer="base/formpage.mako",
    )

    config.add_company_menu(
        parent="third_party",
        order=0,
        label="Clients",
        route_name=COMPANY_CUSTOMERS,
        route_id_key="company_id",
        routes_prefixes=["customer"],
    )

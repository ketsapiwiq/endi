import logging

from endi.utils.widgets import (
    ButtonLink,
    ViewLink,
)

from .routes import COMPANY_CUSTOMERS

logger = logging.getLogger(__name__)


COMPANY_FORM_GRID = (
    (("code", 4),),
    (("company_name", 12),),
    (("civilite", 6),),
    (
        ("lastname", 6),
        ("firstname", 6),
    ),
    (("function", 12),),
    (("address", 12),),
    (
        ("zip_code", 4),
        ("city", 8),
    ),
    (("country", 6),),
    (
        ("tva_intracomm", 6),
        ("registration", 6),
    ),
    (("email", 12),),
    (
        ("mobile", 6),
        ("phone", 6),
    ),
    (("fax", 6),),
    (
        ("compte_cg", 6),
        ("compte_tiers", 6),
    ),
)
INDIVIDUAL_FORM_GRID = (
    (("code", 4),),
    (("civilite", 6),),
    (
        ("lastname", 6),
        ("firstname", 6),
    ),
    (("address", 12),),
    (
        ("zip_code", 4),
        ("city", 8),
    ),
    (("country", 6),),
    (("email", 12),),
    (
        ("mobile", 6),
        ("phone", 6),
    ),
    (("fax", 6),),
    (
        ("compte_cg", 6),
        ("compte_tiers", 6),
    ),
)
INTERNAL_FORM_GRID = (
    (("code", 4),),
    (("company_name", 12),),
    (("source_company_id", 12),),
    (("code", 4),),
    (("civilite", 6),),
    (
        ("lastname", 6),
        ("firstname", 6),
    ),
    (("address", 12),),
    (
        ("zip_code", 4),
        ("city", 8),
    ),
    (("country", 6),),
    (("email", 12),),
    (
        ("mobile", 6),
        ("phone", 6),
    ),
    (("fax", 6),),
    (
        ("compte_cg", 6),
        ("compte_tiers", 6),
    ),
)

FORM_GRIDS = {
    "company": COMPANY_FORM_GRID,
    "individual": INDIVIDUAL_FORM_GRID,
    "internal": INTERNAL_FORM_GRID,
}


def populate_actionmenu(request, context=None):
    """
    populate the actionmenu for the different views (list/add/edit ...)
    """
    company_id = request.context.get_company_id()
    request.actionmenu.add(get_list_view_btn(company_id))
    if context is not None and context.__name__ == "customer":
        request.actionmenu.add(get_view_btn(context.id))


def get_list_view_btn(id_):
    return ButtonLink("Liste des clients", path=COMPANY_CUSTOMERS, id=id_)


def get_view_btn(customer_id):
    return ViewLink(
        "Revenir au client", "view_customer", path="customer", id=customer_id
    )


def get_edit_btn(customer_id):
    return ViewLink(
        "Modifier",
        "edit_customer",
        path="customer",
        id=customer_id,
        _query=dict(action="edit"),
    )

"""
    Task ttc computing tool
    Used to compute invoice, estimation or cancelinvoice totals
"""
import operator
from endi.compute import math_utils
from endi.compute.task import (
    GroupComputeMixin,
    TaskComputeMixin,
    LineComputeMixin,
    DiscountLineMixin,
)


class TaskTtcCompute(TaskComputeMixin):
    """
    class A(TaskTtcCompute):
        pass

    A.total()
    """

    task = None

    def __init__(self, task):
        TaskComputeMixin.__init__(self, task)

    def total_ht(self):
        """
        compute the HT amount
        """
        total_ht = self.groups_total_ht() - self.discount_total_ht()
        return self.floor(total_ht)

    def get_tvas(self):
        """
        return a dict with the tvas amounts stored by tva
        {1960:450.56, 700:45}
        """
        ret_dict = {}
        for group in self.task.line_groups:
            for key, value in list(group.get_tvas().items()):
                val = ret_dict.get(key, 0)
                val += value
                ret_dict[key] = val

        for discount in self.task.discounts:
            val = ret_dict.get(discount.tva, 0)
            val -= discount.tva_amount()
            ret_dict[discount.tva] = val

        for key in ret_dict:
            ret_dict[key] = self.floor(ret_dict[key])
        return ret_dict

    def get_tvas_by_product(self):
        """
        Return tvas stored by product type
        """
        ret_dict = {}
        for group in self.task.line_groups:
            for key, value in group.get_tvas_by_product().items():
                val = ret_dict.get(key, 0)
                val += value
                ret_dict[key] = val

        for discount in self.task.discounts:
            val = ret_dict.get("rrr", 0)
            val += discount.tva_amount()
            ret_dict["rrr"] = val

        for key in ret_dict:
            ret_dict[key] = self.floor(ret_dict[key])
        return ret_dict

    def tva_amount(self):
        """
        Compute the sum of the TVAs amount of TVA
        """
        return self.floor(sum(tva for tva in self.get_tvas().values()))

    def no_tva(self):
        """
        return True if all the tvas are below 0
        """
        ret = True
        for key in self.get_tvas():
            if key >= 0:
                ret = False
                break
        return ret

    def total_ttc(self):
        """
        Compute the TTC total
        """
        return sum(group.total_ttc() for group in self.task.line_groups) - sum(
            discount.total() for discount in self.task.discounts
        )

    def total(self):
        """
        Compute TTC after tax removing
        """
        return self.floor(self.total_ttc())

    def tva_native_parts(self):
        return self.tva_ttc_parts()

    def tva_ht_parts(self):
        """
        Return a dict with the HT amounts stored by corresponding tva value
        dict(tva=ht_tva_part,)
        for each tva value
        """
        ret_dict = {}
        lines = []
        for group in self.task.line_groups:
            lines.extend(group.lines)
        ret_dict = self.add_ht_by_tva(ret_dict, lines)
        ret_dict = self.add_ht_by_tva(ret_dict, self.task.discounts, operator.sub)
        return ret_dict

    def tva_ttc_parts(self):
        """
        Return a dict with TTC amounts stored by corresponding tva
        """
        ret_dict = {}
        ht_parts = self.tva_ht_parts()
        tva_parts = self.get_tvas()

        for tva_value, amount in list(ht_parts.items()):
            ret_dict[tva_value] = amount + tva_parts.get(tva_value, 0)
        return ret_dict


class GroupTtcCompute(GroupComputeMixin):
    """
    Computing tool for group ttc objects
    """

    task_line_group = None

    def __init__(self, task_line_group):
        GroupComputeMixin.__init__(self, task_line_group)

    def total_ttc(self):
        """
        Returns the TTC total for this group
        """
        return sum(line.total() for line in self.task_line_group.lines)


class LineTtcCompute(LineComputeMixin):
    """
    Computing tool for line ttc objects
    """

    task_line = None

    def __init__(self, task_line):
        LineComputeMixin.__init__(self, task_line)

    def unit_ht(self):
        """
        Compute the unit ht value of the current task line based on its ttc
        unit value

        :rtype: float
        """
        unit_ttc = self.task_line.cost or 0
        return math_utils.compute_ht_from_ttc(
            unit_ttc,
            self.task_line.tva,
            float_format=False,
        )

    def total_ht(self):
        """
        Compute the line's ht total

        :rtype: int
        """
        result = self.unit_ht() * self._get_quantity()
        return result

    def tva_amount(self):
        """
        compute the tva amount of a line
        :rtype: int
        """
        return self.total() - self.total_ht()

    def total(self):
        """
        Compute the ttc amount of the line
        :rtype: int
        """
        quantity = self._get_quantity()
        cost = self.task_line.cost or 0
        return cost * quantity


class DiscountLineTtcCompute(DiscountLineMixin):
    """
    Computing tool for discount line ttc objects
    """

    discount_line = None

    def __init__(self, discount_line):
        DiscountLineMixin.__init__(self, discount_line)

    def total_ht(self):
        """
        compute round total ht amount of a line
        """
        total_ttc = self.total()
        result = math_utils.compute_ht_from_ttc(
            total_ttc,
            self.discount_line.tva,
            float_format=False,
        )
        return result

    def tva_amount(self):
        """
        compute the tva amount of a line
        """
        total_ttc = self.total()
        total_ht = self.total_ht()
        return total_ttc - total_ht

    def total(self):
        """
        :return: float ttc amount of a line
        """
        return self.discount_line.amount

import logging
import datetime

from zope.interface import implementer

from endi.interfaces import ITreasuryProducer
from endi.models.expense import ExpenseSheet
from endi.models.expense.sheet import BaseExpenseLine
from endi.compute.math_utils import percentage
from endi.utils.strings import format_account
from .base import (
    MissingData,
    double_lines,
    BaseSageBookEntryFactory,
    format_sage_date,
    filter_accounting_entry,
)

logger = log = logging.getLogger(__name__)


class BaseSageExpenseContribution(BaseSageBookEntryFactory):
    """
    Base contribution line generator used for expenses
    """

    def __init__(self, *args, **kwargs):
        BaseSageBookEntryFactory.__init__(self, *args, **kwargs)
        self.contribution_module = self.get_contribution_module()

    def _has_contribution_module(self):
        return bool(self.contribution_module)

    def _get_contribution_amount(self, ht):
        """
        Return the contribution on the HT total
        """
        return percentage(ht, self.contribution_module.percentage)

    @double_lines
    def _credit_company(self, value, expense_libelle, expense_date):
        """
        Contribution : Crédit entreprise
        """
        entry = self.get_base_entry()

        entry.update(
            compte_cg=self.contribution_module.compte_cg_debit,
            num_analytique=self.company.code_compta,
            credit=value,
            libelle=expense_libelle,
        )
        if expense_date:
            entry["date"] = expense_date
        return entry

    @double_lines
    def _debit_company(self, value, expense_libelle, expense_date):
        """
        Contribution : Débit entreprise
        """
        entry = self.get_base_entry()
        entry.update(
            compte_cg=self._get_config_value("compte_cg_banque"),
            num_analytique=self.company.code_compta,
            debit=value,
            libelle=expense_libelle,
        )
        if expense_date:
            entry["date"] = expense_date
        return entry

    @double_lines
    def _credit_cae(self, value, expense_libelle, expense_date):
        """
        Contribution : Crédit CAE
        """
        entry = self.get_base_entry()
        entry.update(
            compte_cg=self._get_config_value("compte_cg_banque"),
            num_analytique=self._get_config_value("numero_analytique"),
            credit=value,
            libelle=expense_libelle,
        )
        if expense_date:
            entry["date"] = expense_date
        return entry

    @double_lines
    def _debit_cae(self, value, expense_libelle, expense_date):
        """
        Contribution : Débit CAE
        """
        entry = self.get_base_entry()
        entry.update(
            compte_cg=self.contribution_module.compte_cg_credit,
            num_analytique=self._get_config_value("numero_analytique"),
            debit=value,
            libelle=expense_libelle,
        )
        if expense_date:
            entry["date"] = expense_date
        return entry

    def yield_contribution_entries(self, amount, libelle, date=None):
        logger.info("Yielding contribution entries")
        contribution = self._get_contribution_amount(amount)
        logger.info(f"Contribution amount {contribution}")
        yield self._credit_company(contribution, libelle, date)
        yield self._debit_company(contribution, libelle, date)
        yield self._credit_cae(contribution, libelle, date)
        yield self._debit_cae(contribution, libelle, date)


class SageExpenseBase(BaseSageExpenseContribution):
    static_columns = (
        "code_journal",
        "num_feuille",
        "type_",
        "num_endi",
    )
    variable_columns = (
        "compte_cg",
        "num_analytique",
        "compte_tiers",
        "code_tva",
        "debit",
        "credit",
        "libelle",
        "date",
    )

    _label_template_key = "bookentry_expense_label_template"

    def set_expense(self, expense: ExpenseSheet):
        self.expense = expense
        self.company = expense.company

    @property
    def code_journal(self):
        return self.config["code_journal_ndf"]

    @property
    def date(self):
        expense_date = datetime.date(self.expense.year, self.expense.month, 1)
        return format_sage_date(expense_date)

    @property
    def num_feuille(self):
        return "ndf{0}{1}".format(self.expense.month, self.expense.year)

    @property
    def num_endi(self):
        return str(self.expense.official_number)

    @property
    def libelle(self):
        return self._mk_libelle(self.expense, None)

    def get_line_libelle(self, expenseline: "BaseExpenseLine"):
        """Libelle, but for ungroupped exports"""
        # ExpenseKmLine.supplier does not exist
        supplier = getattr(expenseline, "supplier", None)
        # Same for invoice_number
        invoice_number = getattr(expenseline, "invoice_number", None)
        return self._mk_libelle(
            self.expense,
            supplier,
            expenseline.description,
            invoice_number,
        )

    def _mk_libelle(self, sheet, supplier, description="", invoice_number=""):
        # Expose `supplier_name` rather than `supplier` (as on suppplier
        # invoices) because it could crash (supplier is not mandatory for
        # ExpenseLine, and not even present on ExpenseKmLine.
        return self.label_template.format(
            beneficiaire=format_account(sheet.user, reverse=False),
            beneficiaire_LASTNAME=sheet.user.lastname.upper(),
            expense=sheet,
            expense_date=datetime.date(sheet.year, sheet.month, 1),
            expense_description=description,
            invoice_number=invoice_number,
            supplier_label=supplier.label if supplier else "",
            titre=sheet.title if sheet.title else "",
        )


class SageExpenseMain(SageExpenseBase):
    """
    Main module for expense export to sage.
    Should be the only module, but we keep more or less the same structure as
    for invoice exports
    """

    @double_lines
    def _credit(self, ht, expense_libelle, expense_date):
        """
        Main CREDIT The mainline for our expense sheet
        """
        entry = self.get_base_entry()
        entry.update(
            compte_cg=self.company.get_general_expense_account(),
            num_analytique=self.company.code_compta,
            compte_tiers=self.expense.user.compte_tiers,
            credit=ht,
            libelle=expense_libelle,
            date=expense_date,
        )
        return entry

    @double_lines
    def _debit_ht(self, expense_type, ht, expense_libelle, expense_date):
        """
        Débit HT du total de la charge
        """
        entry = self.get_base_entry()
        entry.update(
            compte_cg=expense_type.code,
            num_analytique=self.company.code_compta,
            code_tva=expense_type.code_tva,
            debit=ht,
            libelle=expense_libelle,
            date=expense_date,
        )
        return entry

    @double_lines
    def _credit_tva_on_margin(self, expense_type, tva, expense_libelle, expense_date):
        if expense_type.compte_tva is None:
            raise MissingData(
                "Sage Expense : Missing compte_produit_tva_on_margin          "
                "      in expense_type"
            )

        entry = self.get_base_entry()
        entry.update(
            compte_cg=expense_type.compte_produit_tva_on_margin,
            num_analytique=self.company.code_compta,
            code_tva=expense_type.code_tva,
            credit=tva,
            libelle=expense_libelle,
            date=expense_date,
        )
        return entry

    @double_lines
    def _debit_tva(self, expense_type, tva, expense_libelle, expense_date):
        """
        Débit TVA de la charge
        """
        if expense_type.compte_tva is None:
            raise MissingData("Sage Expense : Missing compte_tva in expense_type")
        entry = self.get_base_entry()
        entry.update(
            compte_cg=expense_type.compte_tva,
            num_analytique=self.company.code_compta,
            code_tva=expense_type.code_tva,
            debit=tva,
            libelle=expense_libelle,
            date=expense_date,
        )
        return entry

    def _write_complete_debit(
        self, expense_type, ht, tva, expense_libelle, expense_date
    ):
        """
        Write a complete debit including VAT and contribution
        """
        if expense_type.tva_on_margin:
            yield self._debit_ht(
                expense_type,
                ht + tva,
                expense_libelle,
                expense_date,
            )
            yield self._credit_tva_on_margin(
                expense_type,
                tva,
                expense_libelle,
                expense_date,
            )

        else:
            yield self._debit_ht(
                expense_type,
                ht,
                expense_libelle,
                expense_date,
            )
        if tva != 0:
            yield self._debit_tva(expense_type, tva, expense_libelle, expense_date)

        if expense_type.contribution and self._has_contribution_module():
            yield from self.yield_contribution_entries(
                ht, expense_libelle, expense_date
            )

    def _yield_grouped_expenses(self, category: str = None):
        """
        yield entries grouped by type
        """
        total = self.expense.get_total(category)
        if not total:
            return
        # An écriture summing all expenses
        yield self._credit(total, self.libelle, self.date)

        # An écriture for every category
        for charge in self.expense.get_lines_by_type(category):
            expense_type = charge[0].expense_type
            ht = sum([line.total_ht for line in charge])
            tva = sum([line.total_tva for line in charge])

            yield from self._write_complete_debit(
                expense_type, ht, tva, self.libelle, self.date
            )

    def _yield_detailed_expenses(self, category: str = None):
        """
        yield entries for each expense line
        """
        # An écriture per expense with all details in order
        for line in self.expense.get_lines(category):
            expense_date = format_sage_date(line.date)

            yield self._credit(
                line.total,
                self.get_line_libelle(line),
                expense_date,
            )

            expense_type = line.expense_type
            ht = line.total_ht
            tva = line.total_tva

            yield from self._write_complete_debit(
                expense_type,
                ht,
                tva,
                self.get_line_libelle(line),
                expense_date,
            )

    def yield_entries(self, category: str = None):
        """
        Yield all the book entries for the current expensesheet
        """
        if self.expense.is_void:
            log.warn("Exporting a void expense : {0}".format(self.expense.id))
            return

        ungroup_expenses = bool(self.config.get("ungroup_expenses_ndf", "0") == "1")
        # Crédits
        if not ungroup_expenses:
            yield from self._yield_grouped_expenses(category)
        else:
            yield from self._yield_detailed_expenses(category)


@implementer(ITreasuryProducer)
class ExpenseExportProducer:
    """
    Export an expense to a Sage
    """

    _default_modules = (SageExpenseMain,)
    use_analytic = True
    use_general = True

    def __init__(self, context, request):
        self.config = request.config
        self.modules = []
        for module in self._default_modules:
            self.modules.append(module(context, request))

    def _get_item_book_entries(self, expense, category=None):
        """
        Return book entries for a single expense
        """
        for module in self.modules:
            module.set_expense(expense)
            for entry in module.yield_entries(category):
                gen_line, analytic_line = entry
                if self.use_general:
                    yield filter_accounting_entry(gen_line)
                if self.use_analytic:
                    yield filter_accounting_entry(analytic_line)

    def get_item_book_entries(self, expenses, category=None):
        return list(self._get_item_book_entries(expenses, category))

    def get_book_entries(self, expenses, category=None):
        """
        Return the book entries for an expenselist
        """
        result = []
        for expense in expenses:
            result.extend(list(self._get_item_book_entries(expense, category)))
        return result

import logging
import datetime

from endi.scripts.utils import (
    command,
    get_value,
    has_value,
)
from endi.models.task import (
    Task,
)
from endi_base.models.base import DBSESSION as db


def refresh_task_amount_command(arguments, env):
    """
    Refresh the task amount cache

    :param dict arguments: docopt parsed arguments
    :param dict env: The pyramid env
    """
    logger = logging.getLogger(__name__)
    logger.debug("Refreshing cached Task amounts")
    session = db()
    index = 0

    date = get_value(arguments, "date", None)
    if date is None:
        date = datetime.date(datetime.date.today().year, 1, 1)
    else:
        date = datetime.datetime.strptime(date, "%Y-%m-%d").date()

    logger.debug("Refreshing tasks dated after {}".format(date))

    query = Task.query()
    query = query.filter(Task.date >= date)
    logger.debug("Refreshing data for {} tasks".format(query.count()))
    for task in query:
        try:
            task.cache_totals()
            index += 1
            if index % 200 == 0:
                logger.debug("flushing")
                session.flush()
        except Exception:
            logger.exception("Error while caching total : {0}".format(task.id))


def refresh_task_pdf_command(arguments, env):
    """
    Refresh task's pdf cached files

    .. code-block: command

        $ endi-cache app.ini refresh_task_pdf --all=1
        $ endi-cache app.ini refresh_task_pdf --companies=1,2

    :param dict arguments: docopt parsed arguments
    :param dict env: The pyramid env
    """
    from endi.models.company import Company
    from endi.models.files import File

    logger = logging.getLogger(__name__)

    session = db()
    allcompanies = has_value(arguments, "all")
    if allcompanies:
        companies = [c[0] for c in session.query(Company.id)]
    else:
        companies = get_value(arguments, "companies", "").split(",")
        companies = [int(id_) for id_ in companies]

    if not companies:
        raise Exception("Missing mandatory --companies argument (or --all)")

    logger.debug("Cleaning cache for {}".format(companies))
    from endi.models.task import Task

    pdf_ids = []
    tasks = (
        Task.query()
        .filter(Task.status == "valid")
        .filter(Task.company_id.in_(companies))
        .filter(Task.pdf_file_id != None)
        .all()
    )

    for task in tasks:
        pdf_ids.append(task.pdf_file_id)
        task.pdf_file = None
        session.merge(task)

    session.flush()

    for file_ in pdf_ids:
        session.delete(File.get(file_))


def purge_pdf_files_command(arguments, env):
    """
    Purge cached pdf files using date timedelta
    """
    from endi.models.files import File
    from endi.models.task import Task

    logger = logging.getLogger(__name__)

    dbsession = db()
    days = get_value(arguments, "days", None)
    if days is None:
        raise Exception("Can't purge everything, not a good idea")

    silent = has_value(arguments, "silent")

    import datetime

    from_day = datetime.date.today() - datetime.timedelta(days=int(days))

    query = Task.query().join(Task.pdf_file).filter(File.created_at >= from_day)

    if silent:
        logger.info("{} tasks' pdf will be cleared".format(query.count()))
    else:
        for task in query:
            dbsession.delete(task.pdf_file)


def sync_sale_product_amount_command(arguments, env):
    """
    Refresh sale catalog amounts

    :param dict arguments: docopt parsed arguments
    :param dict env: The pyramid env
    """
    logger = logging.getLogger(__name__)
    logger.debug("Refreshing sale catalog amounts")

    product_type = get_value(arguments, "type", None)
    if (
        product_type != "base_sale_product"
        and product_type != "work_item"
        and product_type != "all"
    ):
        raise Exception(
            f"Mandatory argument --type missing or wrong {product_type} in {arguments}. \
            Available values : 'base_sale_product', 'work_item', or 'all'."
        )

    if product_type == "base_sale_product" or product_type == "all":
        logger.debug("> Syncing BaseSaleProduct amounts...")
        from endi.models.sale_product.base import BaseSaleProduct

        for p in BaseSaleProduct.query():
            p.sync_amounts()

    if product_type == "work_item" or product_type == "all":
        logger.debug("> Syncing WorkItem amounts...")
        from endi.models.sale_product import WorkItem

        for wi in WorkItem.query():
            wi.sync_amounts()

    logger.debug("Sale catalog amounts refreshed !")


def sync_progress_task_amounts_command(arguments, env):
    """
    Sync amounts on progress invoicing tasks (all or given)

    .. code-block: command

        $ endi-cache app.ini sync_progress_task_amounts
        $ endi-cache app.ini sync_progress_task_amounts --id=10000

    """
    import transaction
    from endi.models.task import Task

    logger = logging.getLogger(__name__)

    cache = {}
    task_id = get_value(arguments, "id", None)
    if task_id is None:
        tasks = Task.query().filter(Task.progress_invoicing_plan != None)
    else:
        tasks = [Task.get(task_id)]
        if tasks[0] is None or tasks[0].progress_invoicing_plan is None:
            raise Exception(
                f"Task {task_id} doesn't exist or is not in progress invoicing mode"
            )

    logger.debug("Synchronisation des montants d'avancement...")
    for t in tasks:
        logger.debug(f" > Synchronisation de {t.official_number} ({t.id})")
        cache[t.id] = [t.ht, t.tva, t.ttc]
        t.progress_invoicing_plan.sync_with_task()
    logger.debug("Fin de la synchronisation")

    transaction.commit()
    transaction.begin()

    logger.debug("Vérification des documents modifiés...")
    if task_id is None:
        tasks = Task.query().filter(Task.progress_invoicing_plan != None)
    else:
        tasks = [Task.get(task_id)]
    for t in tasks:
        ht, tva, ttc = cache.get(t.id)
        if t.ht != ht:
            logger.debug(
                f" > {t.official_number} ({t.id}) : Le HT a changé \
après la synchronisation {ht} => {t.ht}"
            )
    logger.debug("Fin de la vérification")


def cache_entry_point():
    """
    ENDI CACHE UTILITIES

    Usage:
        endi-cache <config_uri> refresh_task_amount [--date=<date>]
        endi-cache <config_uri> refresh_task_pdf [--companies=<companies>] [--all=<allcompanies>]
        endi-cache <config_uri> purge_pdf [--days==<days>] [--silent=<silent>]
        endi-cache <config_uri> sync_sale_product_amount --type=<product_type>
        endi-cache <config_uri> sync_progress_task_amounts [--id=<task_id>]

    o refresh : Ask for a cache refresh

    Options:
        -h --help               Show this screen
        --date=<date>           Refresh all task dated after date
        --companies=<companies> Comma separated list of company ids
        --all=<allcompanies>    set value to 1 to treat all companies
        --days=<days>           days gap from which to purge
        --silent=<silent>       set a value it you only want the count
        --type=<product_type>   Product type : 'base_sale_product', 'work_item', or 'all'
        --id=<task_id>          Task ID (all progress invoicing tasks if missing)
    """

    def callback(arguments, env):
        if arguments["refresh_task_amount"]:
            func = refresh_task_amount_command
        elif arguments["refresh_task_pdf"]:
            func = refresh_task_pdf_command
        elif arguments["purge_pdf"]:
            func = purge_pdf_files_command
        elif arguments["sync_sale_product_amount"]:
            func = sync_sale_product_amount_command
        elif arguments["sync_progress_task_amounts"]:
            func = sync_progress_task_amounts_command
        return func(arguments, env)

    try:
        return command(callback, cache_entry_point.__doc__)
    finally:
        pass

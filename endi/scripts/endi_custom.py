import csv
import logging

import transaction
from zope.sqlalchemy import mark_changed
from endi.scripts.utils import (
    command,
    get_value,
)
from endi.models.node import Node
from endi_base.models.base import DBSESSION as db


logger = logging.getLogger("endi")


def _get_company_id(third_party_id):
    return (
        db()
        .execute(
            f"""SELECT
    company_id
    FROM 
    third_party
    where id={third_party_id}"""
        )
        .scalar()
    )


def _get_user_id(third_party_id):
    company_id = _get_company_id(third_party_id)
    return (
        db()
        .execute(
            f"""SELECT
    max(account_id)
    FROM 
    company_employee
    where company_id={company_id}"""
        )
        .scalar()
    )


def import_third_party_comments_command(arguments, env):
    from endi.models.status import StatusLogEntry

    filepath = get_value(arguments, "filepath")
    if not filepath:
        raise Exception("Le chemin vers le fichier est requis")

    for s in (
        StatusLogEntry.query()
        .join(Node)
        .filter(Node.type_.in_(["supplier", "customer"]))
    ):
        db().delete(s)
    db().flush()

    with open(filepath, "r") as file_buffer:
        csv_data = csv.DictReader(
            file_buffer, fieldnames=["id", "company_id", "comments"]
        )
        csv_data.__next__()
        for line in csv_data:
            id_, comment = line["id"], line["comments"]
            user_id = _get_user_id(id_)
            entry = StatusLogEntry(
                node_id=id_,
                comment=comment.replace("   ", "\n"),
                user_id=user_id,
                label="Commentaires",
                visibility="public",
                pinned=True,
            )
            db().add(entry)
            db().flush()


def clean_file_requirements_645_command(arguments, env):
    columns = ["doctype", "requirement_type", "node_id", "file_type_id"]
    where_clause = " AND ".join([f"a.{col} = b.{col}" for col in columns])
    query_cols = ",".join(columns)
    session = db()
    str_query = f"""
        select id, {query_cols}, file_id, validation from sale_file_requirement as a
        where exists (
            select 1
            from sale_file_requirement as b
            where {where_clause}
            LIMIT 1,1
        ) ORDER BY node_id, file_type_id, file_id desc, validation desc
    """

    query = session.execute(str_query)
    result = list(query.fetchall())
    cache = {}
    for entry in result:
        key = entry.doctype, entry.requirement_type, entry.node_id, entry.file_type_id
        cache.setdefault(key, []).append(entry)

    for key, entries in cache.items():
        count = len(entries)
        for index, entry in enumerate(entries):
            if not entry.file_id or index < count - 1:
                session.execute(
                    f"delete from sale_file_requirement where id={entry.id}"
                )
                break
    mark_changed(session)


def _check_plan_task_totals(plan, amount_cache):
    task = plan.task

    if task.id in amount_cache and task.status == "valid":
        old_totals = amount_cache.get(task.id)
        if old_totals["ht"] != task.total_ht():
            logger.info(
                f"    - La facture {task.id} a un montant total différent de ce "
                f"qu'elle avait avant migration"
                f" HT précédent {old_totals['ht']} Nouveau HT "
                f": {task.ht}"
            )
        else:
            logger.info(
                f"   + La facture {task.id} a désormais le même total HT qu'avant migration {old_totals['ht']} {task.total_ht()} {task.ht}"
            )


def clean_avancement_647_command(args, env):
    """
    Nettoye l'avancement après migration 6.4
    """

    from endi.models.project import Business
    from endi.models.progress_invoicing import ProgressInvoicingPlan

    ids = [b.id for b in Business.query().filter_by(invoicing_mode="progress")]
    dbsession = db()
    amount_cache: dict = {}
    logger.setLevel(logging.INFO)
    for business_id in ids:

        # transaction.begin()
        business = Business.get(business_id)
        logger.info(f"Traitement de l'affaire {business.id}")
        for invoice in business.invoices:
            if invoice.ht != invoice.total_ht():
                logger.warn(
                    f"   + Avant nettoyage la facture {invoice.id} a un Ht "
                    f"en cache de {invoice.ht} alors que le total calculé "
                    f"est de {invoice.total_ht()}"
                )
                amount_cache[invoice.id] = {
                    "ttc": invoice.ttc,
                    "ht": invoice.ht,
                    "tva": invoice.tva,
                }
        plans = ProgressInvoicingPlan.query().filter_by(business_id=business.id)
        logger.debug("  + Nettoyage des produits créés en trop par la migration")
        for plan in plans:
            for chapter in plan.chapters:
                products = chapter.products
                for product in products:
                    if product.task_line_id is None:
                        dbsession.delete(product)
        dbsession.flush()

    mark_changed(dbsession)
    transaction.commit()
    transaction.begin()
    dbsession = db()
    for business_id in ids:
        plans = ProgressInvoicingPlan.query().filter_by(business_id=business_id)
        logger.info("  + Synchronisation des plans d'avancement avec les Factures")
        for plan in plans:
            if plan.task.status != "valid":
                plan.sync_with_task()
                _check_plan_task_totals(plan, amount_cache)
            # dbsession.merge(plan.task)
        # transaction.commit()
        # transaction.begin()
        plans = ProgressInvoicingPlan.query().filter_by(business_id=business_id)
        for plan in plans:
            for chapter in plan.chapters:
                for product in chapter.products:
                    product.already_invoiced = product.status.invoiced_percentage(
                        product=product
                    )
                    dbsession.merge(product)
        dbsession.flush()
    mark_changed(dbsession)
    transaction.commit()


def clean_avancement_duplicate_648_command(args, env):
    dbsession = db()
    from endi.models.progress_invoicing import ProgressInvoicingPlan

    logger.info("Lancement du nettoyage des Plans d'avancement")
    logger.info("Suppression des TaskLineGroup orphelins")
    dbsession.execute("Delete from task_line_group where task_id is null")
    dbsession.execute(
        "Delete from progress_invoicing_base_product where task_line_id is null"
    )
    mark_changed(dbsession)
    transaction.commit()
    transaction.begin()
    dbsession = db()
    for plan in ProgressInvoicingPlan.query():
        for chapter in plan.chapters:
            if chapter.task_line_group.task_id != plan.task_id:
                dbsession.delete(chapter)
            else:
                for product in chapter.products:
                    if product.task_line.group_id != chapter.task_line_group.id:
                        logger.info(
                            f"Suppression d'un produit associé au "
                            f"ProgressInvoicingBaseStatus {product.base_status_id}"
                        )
                        dbsession.delete(product)

    logger.info(
        "Les duplicatas ont été supprimés, veuillez lancez la commande "
        "clean_avancement_647 pour resetter correctement les already_invoiced"
        " de chaque élément"
    )


def load_edp_from_json_command(args, env):
    import json
    import datetime
    from endi.models.task import Estimation
    from endi.models.project import Project
    from endi.models.user.user import User
    from endi.models.price_study import (
        PriceStudyDiscount,
        PriceStudyProduct,
        PriceStudyWork,
        PriceStudyWorkItem,
        PriceStudy,
    )

    session = db()
    filename = get_value(args, "filepath")
    if not filename:
        raise Exception("Nom de fichier manquant")

    with open(filename, "r") as f:
        data = json.load(f)

    request = env["request"]
    request.dbsession = session
    edp_ids = []
    untreated = []
    devis_ids = []
    for edp in data:
        project = Project.get(edp["project_id"])
        company = project.company
        owner = User.get(edp["owner_id"])
        if not project or not company or not project.customers:
            untreated.append(edp)
            logger.warn(
                f"L'edp {edp} n'a pas pu être restaurée, il manque des clients, des projets ou des enseignes"
            )
            continue
        customer = project.customers[0]
        devis = Estimation.create(
            request,
            customer,
            {
                "project": project,
                "company": company,
                "business_type_id": project.project_type.default_business_type.id,
                "user": owner,
                "date": datetime.datetime.strptime(
                    edp["created_at"], "%Y-%m-%dT%H:%M:%S"
                ),
            },
        )
        devis_ids.append(devis.id)
        price_study = devis.set_price_study(request)
        chapter = price_study.chapters[0]
        general_overhead = set()
        for product_data in edp["products"]:
            product = PriceStudyProduct(
                chapter=chapter,
            )
            # Base sale product
            for key in (
                "margin_rate",
                "ht",
                "description",
                "unity",
                "quantity",
                "total_ht",
                "tva_id",
                "product_id",
            ):
                if product_data.get(key):
                    setattr(product, key, product_data[key])
            setattr(product, "order", product_data.get("`order`", 0))
            # product
            if product_data["supplier_ht"]:
                product_data["mode"] = "supplier_ht"
            else:
                product_data["mode"] = "ht"
            for key in ("mode", "supplier_ht", "base_sale_product_id"):
                if product_data.get(key):
                    setattr(product, key, product_data[key])
            session.add(product)
            session.flush()
            if product_data["general_overhead"]:
                general_overhead.add(product_data["general_overhead"])

        for work_data in edp["works"]:
            work = PriceStudyWork(chapter=chapter)
            # Base sale product
            for key in (
                "margin_rate",
                "ht",
                "description",
                "unity",
                "quantity",
                "total_ht",
                "tva_id",
                "product_id",
            ):
                if work_data.get(key):
                    setattr(work, key, work_data[key])
            # Work
            setattr(work, "title", work_data["title"])
            session.add(work)
            session.flush()
            if work_data["general_overhead"]:
                general_overhead.add(work_data["general_overhead"])
            for item_data in work_data["items"]:
                item = PriceStudyWorkItem(price_study_work=work)
                if item_data["supplier_ht"]:
                    item_data["mode"] = "supplier_ht"
                else:
                    item_data["mode"] = "ht"
                for key in (
                    "description",
                    "ht",
                    "supplier_ht",
                    "mode",
                    "unity",
                    "work_unit_ht",
                    "work_unit_quantity",
                    "quantity_inherited",
                    "work_unit_ht",
                    "total_ht",
                    "base_sale_product_id",
                ):
                    if item_data.get(key):
                        setattr(item, key, item_data[key])

                session.add(item)
                session.flush()

        if len(general_overhead) > 0:
            if len(general_overhead) > 1:
                logger.warn(
                    f"!!! L'edp {price_study.id} avait plusieurs coeff de "
                    f"frais généraux différents avant migration"
                )
            price_study.general_overhead = general_overhead[0]
            session.merge(price_study)
        for discount_data in edp["discounts"]:
            discount = PriceStudyDiscount(price_study=price_study)
            for key in ("description", "amount", "percentage", "type_"):
                setattr(discount, key, discount_data.get(key))
            discount.order = discount_data["`order`"]
            session.add(discount)
            session.flush()
        edp_ids.append(price_study.id)
    mark_changed(session)
    transaction.commit()
    transaction.begin()
    session = db()
    request.dbsession = session
    for price_study_id in edp_ids:
        p: PriceStudy = PriceStudy.get(price_study_id)
        p.sync_amounts(sync_down=True)
        p.sync_with_task(request)
    mark_changed(session)
    logger.info("Les devis suivants ont été créés")
    logger.info(devis_ids)
    if untreated:
        for edp in untreated:
            logger.warn(
                f"Une edp n'a pas pu être restaurée, il manque le projet, "
                f"l'enseigne ou plus probablement le projet {edp['project_id']} n'a "
                f"pas de client "
                f"récupérer le json ci-dessous, ajoutez un client au projet et relancez le "
                f"script avec uniquement ces données "
            )
        logger.info(json.dumps(untreated))


def custom_entry_point():
    """Préparation de la migration 6.4

    Usage:
        endi-custom <config_uri> prepare_migration_64
        endi-custom <config_uri> import_third_party_comments [--filepath=<filepath>]
        endi-custom <config_uri> clean_file_requirements_645
        endi-custom <config_uri> clean_avancement_647
        endi-custom <config_uri> clean_avancement_duplicate_648
        endi-custom <config_uri> load_edp_from_json [--filepath=<filepath>]


    o prepare_migration_64 : Preparation de la migration des edp pour la 6.4
    o import_third_party_comments : Importe un fichier csv avec des données récupérées depuis la base avec >>> echo "SELECT CONCAT('\\"', id, '\\",\\"', company_id, '\\",\\"',REPLACE(comments, '\\n', '   '),'\\"') FROM third_party where comments is not NULL;" | mysql -uroot endi > fichier.csv
    o clean_file_requirements_645 : Nettoie les fichiers requis qui ont pu être créés en double entre la mise à jour 6.4 et la publication de la 6.4.5
    o clean_avancement_647 : Nettoye l'avancement pour assurer le contenu présenté
    o clean_avancement_duplicate_648 : Nettoyage des duplicatas dans l'avancement
    o load_edp_from_json : Charge des edps depuis un fichier json

    Options:
        -h --help               Show this screen
        --filepath=<filepath>   Chemin vers le fichier à importer
    """

    def callback(arguments, env):
        if arguments["import_third_party_comments"]:
            func = import_third_party_comments_command
        elif arguments["clean_file_requirements_645"]:
            func = clean_file_requirements_645_command
        elif arguments["clean_avancement_647"]:
            func = clean_avancement_647_command
        elif arguments["clean_avancement_duplicate_648"]:
            func = clean_avancement_duplicate_648_command
        elif arguments["load_edp_from_json"]:
            func = load_edp_from_json_command
        return func(arguments, env)

    try:
        return command(callback, custom_entry_point.__doc__)
    finally:
        pass

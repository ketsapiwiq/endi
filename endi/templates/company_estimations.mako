<%inherit file="/estimations.mako" />

<%block name='actionmenucontent'>
    <div class='layout flex main_actions'>

    % if api.has_permission('add.estimation'):

	<div class='layout flex main_actions'>
		<a class='btn btn-primary' title="Ajouter un nouveau devis"
           href="${request.route_path('company_estimations', id=request.context.id, _query=dict(action='add'))}"
        >
            ${api.icon('plus')}
            Ajouter un devis
		</a>
	</div>
    % endif
    </div>
</%block>

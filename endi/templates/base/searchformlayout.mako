<%doc>
Search form layout template
</%doc>

<%def name="searchform()">
    % if form is not UNDEFINED and form:
        <div class='collapsible search_filters'>
            <h2 class='collapse_title'>
            <a href='#filter-form' data-toggle="collapse" aria-expanded="true" aria-controls="filter-form" accesskey="R">
                <span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#search"></use></svg></span>
                Recherche
                <svg class="arrow"><use href="${request.static_url('endi:static/icons/endi.svg')}#chevron-down"></use></svg>
            </a>
            % if '__formid__' in request.GET:
                <span class='help_text'>
                    <small><em>Des filtres sont actifs</em></small>
                </span>
                <span class='help_text'>
                    <a href="${request.current_route_path(_query={})}">
                        <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#times"></use></svg> Supprimer tous les filtres
                    </a>
                </span>
            % endif
            </h2>
            <div class='collapse_content'>
                <div id='filter-form' class='collapse in' aria-expanded='true'>
                    ${form|n}
                </div>
            </div>
        </div>
    % endif
</%def>

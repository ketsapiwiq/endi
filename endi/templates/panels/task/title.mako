<h1 class='${css}'>
    ${title}
    % if status_label:
    <small>(${status_label})</small>
    % endif
</h1>

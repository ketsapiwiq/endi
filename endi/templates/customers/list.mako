<%inherit file="${context['main_template'].uri}" />
<%namespace file="/base/utils.mako" import="dropdown_item"/>
<%namespace file="/base/pager.mako" import="pager"/>
<%namespace file="/base/pager.mako" import="sortable"/>
<%namespace file="/base/searchformlayout.mako" import="searchform"/>

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
    ${request.layout_manager.render_panel('action_buttons', links=stream_main_actions())}
    ${request.layout_manager.render_panel('action_buttons', links=stream_more_actions())}
</div>
</%block>

<%block name='content'>

${searchform()}

<div>
    <div>
        ${records.item_count} Résultat(s)
    </div>
    <div class='table_container'>
    	% if records:
        <table class="hover_table">
            <thead>
                <tr>
                    <th scope="col" class="col_date">${sortable("Créé le", "created_at")}</th>
                    <th scope="col">${sortable("Code", "code")}</th>
                    <th scope="col" class="col_text">${sortable("Nom du client", "label")}</th>
                    <th scope="col" class="col_text">${sortable("Nom du contact principal", "lastname")}</th>
                    <th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
                </tr>
            </thead>
            <tbody>
        % else:
        <table>
        	<tbody>
				<tr>
					<td colspan='5' class="col_text">
						<em>Aucun client n’a été référencé</em>
					</td>
				</tr>
		% endif
			% for customer in records:
				<tr class='tableelement' id="${customer.id}">
					<% url = request.route_path("customer", id=customer.id) %>
					<% onclick = "document.location='{url}'".format(url=url) %>
					<% tooltip_title = "Cliquer pour voir ou modifier le client « " + customer.label + " »" %>
					<td class="col_date"  onclick="${onclick}" title="${tooltip_title}">${api.format_date(customer.created_at)}</td>
					<td onclick="${onclick}" title="${tooltip_title}">${customer.code}</td>
					<td class="col_text" onclick="${onclick}" title="${tooltip_title}">
						% if customer.archived:
							<small title="Ce client a été archivé"><span class='icon'>${api.icon("archive")} Client archivé</span></small><br />
						% endif
						${customer.label}
						% if customer.is_internal():
						&nbsp;(interne à la CAE)
						% endif
					</td>
					<td class="col_text" onclick="${onclick}" title="${tooltip_title}">
						${customer.get_name()}
					</td>
					${request.layout_manager.render_panel('action_buttons_td', links=stream_actions(customer))}
				</tr>
			% endfor
            </tbody>
        </table>
	</div>
	${pager(records)}
</div>
</%block>

<%block name='footerjs'>
$(function(){
    $('input[name=search]').focus();
});
</%block>

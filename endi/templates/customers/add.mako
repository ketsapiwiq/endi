<%inherit file="${context['main_template'].uri}" />

<%block name='content'>
<div class='layout flex two_cols quarter_reverse'>
    <div>
        <h3>Type de client</h3>
        <div class="radio">
            % for form_name, form in forms:
            <div class="radio-inline">
                <label>
                    <input type='radio' name='customer_form_name' value='${form_name}' 
                    % if form_name == current_form:
                        checked
                    % endif
                    autocomplete='off'
                    >
                    <span>${form.title}</span>
                </label>
            </div>
            % endfor
        </div>

        % for form_name, form in forms:
        
        <div 
            id="${form_name}-container"
            class="customer-form-container"
            % if form_name != current_form:
            style="display:none"
            % endif
            >
            ${form.render()|n}
        </div>
        % endfor
    </div>
    <div class='context_help'>
        <h4>Codes client utilisés</h4>
        <ul>
            % for customer in customers:
                <li>${customer.code.upper()} (${customer.label})</li>
            % endfor
        </ul>
    </div>
</div>
</%block>
<%block name='footerjs'>
$("input[name='customer_form_name']").on('change', (event) => {
    const form_name = event.currentTarget.value;
    $('.customer-form-container').hide();
    $('#' + form_name + "-container").show();
});
</%block>